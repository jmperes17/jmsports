<?php

function saidaEstoqueHistoricoProdutoMateriais($numeroPedido, $observacao = "Venda de material didático pelo portal")
    {
        $CI = get_instance();
        $CI->load->model('m_global');
        $CI->load->library('curl_http');

        $vendaMateriais = $CI->m_global->getQueryAllWhereArraySingle("loja_venda_materiais_didaticos_produtos", "id_loja_venda_materiais_didaticos_produtos", ["numero_pedido" => $numeroPedido]);
        $vendaMateriaisProdutoArray = $CI->m_global->getQueryAllWhereArray("loja_carrinho", "*", ["numero_pedido" => $numeroPedido, "status" => 2]);

        $materialDidaticoArray2 = [];
        $materialDidaticoArray = [];

        foreach ($vendaMateriaisProdutoArray as $vendaMateriaisArray) {

            $tipoProduto = 1; // MATERIAL DIDÁTICO

            // IDENTIFICANDO SE O MATERIAL É UM KIT PROMO
            if (strpos($vendaMateriaisArray->nome_material, "KIT") !== false) {
                $kitPromo = $CI->m_global->getQueryAllWhereArraySingle("kit_promo", "livro_1, livro_2", ["nome" => $vendaMateriaisArray->nome_material]);

                $materialDidaticoArray = $CI->m_global->getQueryAllWhereArraySingle("materiais_didaticos", "id_materiais_didaticos", ["nome_material" => $kitPromo->livro_1]);
                $materialDidaticoArray2 = $CI->m_global->getQueryAllWhereArraySingle("materiais_didaticos", "id_materiais_didaticos", ["nome_material" => $kitPromo->livro_2]);
            } else {
                if ($vendaMateriaisArray->material_didatico_id > 0) {
                    $materialDidaticoArray = $CI->m_global->getQueryAllWhereArraySingle("materiais_didaticos", "id_materiais_didaticos", ["nome_material" => $vendaMateriaisArray->nome_material]);
                } else {
                    if ($vendaMateriaisArray->loja_produto_id > 0) {
                        $tipoProduto = 2; // PRODUTO DE LOJA
                        $materialDidaticoArray = $CI->m_global->getQueryAllWhereArraySingle("loja_produtos", "id_loja_produtos", ["id_loja_produtos" => $vendaMateriaisArray->loja_produto_id]);
                    }
                }
            }

            // historico estoque movimentacao
            if (!empty($materialDidaticoArray)) {
                if ($tipoProduto == 1) {
                    $estoqueMaterial = $CI->m_global->getQueryAllWhereArraySingle("estoque_materiais", "id_estoque_materiais", ["materiais_didaticos_id" => $materialDidaticoArray->id_materiais_didaticos]);
                } else {
                    $estoqueMaterial = $CI->m_global->getQueryAllWhereArraySingle("estoque_materiais", "id_estoque_materiais", ["loja_produto_id" => $materialDidaticoArray->id_loja_produtos]);
                }

                $CI->m_global->insertTableMysql("estoque_movimentacao", [
                    "saida" => 1, "observacao" => "$observacao", "unidade_id" => $vendaMateriaisArray->unidade_id, "data_hora" => date("Y-m-d H:i:s"),
                    "estoque_material_id" => $estoqueMaterial->id_estoque_materiais, "venda_material_didatico_id" => 0, "loja_venda_materiais_didaticos_produtos_id" => $vendaMateriais->id_loja_venda_materiais_didaticos_produtos
                ]);

                if (!empty($materialDidaticoArray2)) {
                    $estoqueMaterial2 = $CI->m_global->getQueryAllWhereArraySingle("estoque_materiais", "id_estoque_materiais", ["materiais_didaticos_id" => $materialDidaticoArray2->id_materiais_didaticos]);
                    $CI->m_global->insertTableMysql("estoque_movimentacao", [
                        "saida" => 1, "observacao" => "$observacao", "unidade_id" => $vendaMateriaisArray->unidade_id, "data_hora" => date("Y-m-d H:i:s"),
                        "estoque_material_id" => $estoqueMaterial2->id_estoque_materiais, "venda_material_didatico_id" => 0, "loja_venda_materiais_didaticos_produtos_id" => $vendaMateriais->id_loja_venda_materiais_didaticos_produtos
                    ]);
                }
            }
        }
    }
