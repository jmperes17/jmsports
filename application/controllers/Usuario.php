<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuario extends CI_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();



        $this->load->model('M_global');

        $this->load->helper('url');
    }

    public function entrar()
    {

        $loginDigitado = $_POST['email'];
        $senhaDigitado = $_POST['senha'];

        $this->db->select('*');
        $this->db->from('clientes');
        $this->db->where('email', $loginDigitado);
        $this->db->where('senha', $senhaDigitado);
        $query = $this->db->get();
        $result = $query->result_array();

        if (!empty($result)) {
            $data = array();

            $usuario_logado = array(
                'logado' => 'sim',
                'usuario' => $loginDigitado,
                'senha' => $senhaDigitado
            );
            #$data = $this->session->set_userdata('usuario_logado', $result);
            // pega o primeiro usuário
            $user = $result[0];
            session_start();
            $_SESSION['logged_in'] = true;
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['user_name'] = $user['name'];

            redirect(base_url('admin/home'));
        } else {
            redirect(base_url('admin/index?usuario_invalido=true'));
        }
    }
    public function sair()
    {
        $data = array();
        session_start();
        $data = session_unset();
        $data = session_destroy();

        $this->load->view('admin/login', $data);
    }
}
