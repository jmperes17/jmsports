<?php
date_default_timezone_set('America/Sao_Paulo');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();



        $this->load->model('M_global');
        $this->load->model('M_relatorios');

        $this->load->helper('url');
    }

    public function index()
    {
        $dados = array();

        $dados['titulo'] = 'Admin';

        $this->load->view('admin/login');
    }

    public function home()
    {
        $dados = array();

        $dados['titulo'] = 'Home';
        #$dados['menu'] = $this->load->view('menu');
        #$dados['footer'] = $this->load->view('footer');

        $this->load->view('admin/home', $dados);
    }
    public function entrar()
    {

        if (!empty($_POST['email']) && !empty($_POST['senha'])) {

            $loginDigitado = $_POST['email'];
            $senhaDigitado = $_POST['senha'];
        } else {
            redirect(base_url('admin/index?usuario_nao_logado=true'));
        }


        $this->db->select('*');
        $this->db->from('clientes');
        $this->db->where('email', $loginDigitado);
        $this->db->where('senha', $senhaDigitado);
        $query = $this->db->get();
        $result = $query->result_array();

        if (!empty($result)) {
            #$data = array();

            #$usuario_logado = array('logado' => 1, 'usuario' => $loginDigitado, 'senha' => $senhaDigitado);
            #$data = $this->session->set_userdata('usuario_logado', $result);

            // pega o primeiro usuário
            $user = $result[0];
            session_start();
            $_SESSION['logged_in'] = true;
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['user_name'] = $user['nome'];

            $nivel = $_SESSION['level'] = $user['level'];

            if ($nivel == 3) {
                redirect(base_url(''));
            } else {
                redirect(base_url('admin/home'));
            }
        } else {

            redirect(base_url('admin/index?usuario_invalido=true'));
        }
    }
    public function sair()
    {
        session_start();

        session_unset();
        session_destroy();

        $this->load->view('admin/login');
    }
    public function gerenciar_vendas()
    {

        session_start();
        $nivel = $_SESSION['level'];

        if ($nivel == 3) {
            redirect(base_url(''));
        }

        $data = array();

        $data['titulo'] = 'Gerenciar Vendas';

        $data['vendasArray'] = $this->M_global->listaVendas();

        $this->load->view('admin/gerenciar_vendas', $data);
    }

    public function acoesEmMassa()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('numero_pedido', 'numero_pedido', 'required');

        session_start();
        $userId = $_SESSION["user_id"];

        if ($this->form_validation->run() == TRUE) {
            $selectAll = [];
            if ($this->input->post("select_all")) {
                $selectAll = $this->input->post("select_all");
                if (!empty($selectAll)) {
                    if ($this->input->post("alteracao")) {

                        foreach ($selectAll as $selects) {

                            $vendaNumeroPedido = $selects;

                            $arrayUpdateMaterialDidatico = ['status_venda' => $this->input->post("alteracao")];

                            $this->M_global->updateTableMysqlWhereArray("vendas", "numero_pedido", $vendaNumeroPedido, $arrayUpdateMaterialDidatico);

                            // Histórico ----------------
                            $this->M_global->insertTableMysql("vendas_historico", ["numero_pedido" => $vendaNumeroPedido, "observacao" => $this->input->post("alteracao"), "data_hora_criacao" => date("Y-m-d H:i:s"), "cliente_id" => $userId]);

                            /* 
                           $justificativa = $this->input->post("justificativa");

                            if (strlen($justificativa) > 2) {
                                $this->m_global->insertTableMysql("venda_materiais_didaticos_historico", ["venda_materiais_didaticos_id" => $vendaNumeroPedido, "observacao" => $justificativa, "data_hora_criacao" => date("Y-m-d H:i:s"), "client_id" => $this->user->user_data->id_clients]);
                            }
                            */
                            // ------------------------------

                        }
                    }
                }
            }
            redirect(base_url("admin/gerenciar_vendas?pedidos_atualizados=true"), 'refresh');
        }
    }
    public function exporta_dados()
    {

        $this->data['vendasArray'] = $this->M_global->listaVendas();
        $this->load->view('admin/exporta_dados', $this->data);
    }

    public function usuarios()
    {
        $data['titulo'] = 'Usuarios';
        $data['usuariosArray'] = $this->M_global->listaUsuarios();
        $this->load->view('admin/listar_usuarios', $data);
    }
    public function clientes()
    {
        $data['titulo'] = 'Clientes';
        $data['usuariosArray'] = $this->M_global->listaClientes();
        $this->load->view('admin/listar_clientes', $data);
    }

    public function editar_funcionario()
    {
        $id = $_REQUEST['id'];
        $data['titulo'] = 'Usuarios';
        $data['funcionariosArray'] = $this->M_global->getQueryAllRows('clientes', 'id', $id);
        #$this->M_global->updateTableMysqlWhereArray('clientes', 'id', $id);
        $this->load->view('admin/editar_usuario', $data);
    }

    public function update_funcionario()
    {
        $arrayData = array(
            'nome' => $_POST['nome'],
            'cpf' => $_POST['cpf'],
            'email' => $_POST['email'],
            'telefone' => $_POST['telefone']
        );

        $id = $_REQUEST['id'];
        if ($this->M_global->updateTableMysqlWhereArray('clientes', 'id', $id, $arrayData)) {
            echo "<script>alert('Dados salvos!');</script>";
            echo "<script>window.location.href='usuarios';</script>";
        }
    }

    public function deletar_funcionario()
    {

        $id = $_REQUEST['id'];
        if ($this->M_global->deleteTableMysql('clientes', 'id', $id)) {
            echo "<script>alert('Dados deletados!');</script>";
            echo "<script>window.location.href='usuarios';</script>";
        }
    }

    public function cadastrar_funcionario()
    {
        $data['titulo'] = 'Cadastrar';

        $this->load->view('admin/cadastrar_funcionario', $data);
    }

    public function cadastrar_usuario()
    {
        $data['titulo'] = 'Cadastre-se';

        $this->load->view('admin/cadastro', $data);
    }

    public function insere_funcionario()
    {
        $arrayData = array(
            'nome' => $_POST['nome'],
            'cpf' => $_POST['cpf'],
            'email' => $_POST['email'],
            'senha' => $_POST['senha'],
            'telefone' => $_POST['telefone'],
            'level' => 2,
            'endereco' => $_POST['endereco'],
            'cep' => $_POST['cep'],
        );

        if ($this->M_global->insertTableMysql('clientes', $arrayData)) {
            echo "<script>alert('Dados salvos!');</script>";
            echo "<script>window.location.href='usuarios';</script>";
        }
    }

    public function insere_usuario()
    {
        $arrayData = array(
            'nome' => $_POST['nome'],
            'cpf' => $_POST['cpf'],
            'email' => $_POST['email'],
            'senha' => $_POST['senha'],
            'telefone' => $_POST['telefone'],
            'level' => 3,
            'endereco' => $_POST['endereco'],
            'cep' => $_POST['cep'],
            'criado' => date('Y-m-d h:i:s')
        );

        $arrayUsuario = $this->M_global->listaUsuariosCadastrados($_POST['cpf']);

        if (empty($arrayUsuario)) {
            if ($this->M_global->insertTableMysql('clientes', $arrayData)) {
                echo "<script>alert('Dados salvos!');</script>";
                echo "<script>window.location.href='index';</script>";
            }
        } else {
            echo "<script>alert('Usuário já existente!');</script>";
            echo "<script>window.location.href='index';</script>";
        }
    }

    public function relatorio_estoque()
    {
        $data['titulo'] = 'Estoque';
        $data['estoqueArray'] = $this->M_global->listaEstoqueMateriais();

        $this->load->view('admin/relatorio_estoque', $data);
    }

    public function estoque_movimentacao()
    {
        $data['titulo'] = 'Estoque - Movimentação';

        $data['estoqueMovimentacao'] = $this->M_global->listaEstoqueMovimentacao();
        $this->load->view('admin/estoque_movimentacao', $data);
    }

    public function cadastrar_produto()
    {
        $data['titulo'] = 'Cadastrar produto';
        $this->load->view('admin/cadastrar_produto', $data);
    }

    public function cadastrando_produto()
    {
        $arquivo1 = null;


        // Set preference for file Upload
        if (!empty($_FILES["imagem"]['name'])) {
            $config['upload_path'] = 'assets/uploads/product_images';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|doc|docx|pdf|xlx|xlsx|zip|rar';
            $config['max_size'] = '956140174'; // max_size in kb
            $config['remove_spaces'] = TRUE;  //it will remove all spaces
            $config['encrypt_name'] = TRUE;   //it wil encrypte the original file name

            //Load upload library
            $this->load->library('upload', $config);


            // File upload
            if ($this->upload->do_upload("imagem")) {

                $uploadData = $this->upload->data();
                if (!empty($uploadData)) {
                    $arquivo1 = $uploadData['file_name'];
                }
            }
        }

        $arrayProdutos = array(
            "imagem" => $arquivo1,
            "nome" => $_POST['nome'],
            "descricao" => $_POST['descricao'],
            "preco" => $_POST['preco'],
            "marca" => $_POST['marca'],
            "tamanho" => $_POST['tamanho'],
            'criado' => date('Y-m-d h:i:s'),
            'modificado' => date('Y-m-d h:i:s')
        );



        if (!empty($arrayProdutos)) {
            $insereProduto = $this->M_global->insertTableMysql('produtos', $arrayProdutos);

            $getIdProduto = $this->M_global->getQueryAllRows("produtos", "nome", $_POST['nome']);
            $produtoId = $getIdProduto[0]->id;

            $arrayEstoque = array(
                "nome_material" => $_POST['nome'],
                "loja_produto_id" => $produtoId
            );

            $arrayMovimentacao = array(
                "entrada" => $_POST['quantidade'],
                "saida" => 0,
                "observacao" => "Entrada manual",
                "data_hora" => date('Y-m-d h:i:s'),
                "loja_produto_id" => $produtoId
            );

            // insere entrada no estoque
            $this->M_global->insertTableMysql('estoque_materiais', $arrayEstoque);

            // insere entrada no estoque_movimentacao
            $this->M_global->insertTableMysql('estoque_movimentacao', $arrayMovimentacao);

            echo "<script>alert('Dados salvos!');</script>";
            echo "<script>window.location.href='cadastrar_produto';</script>";
        }
    }

    public function relatorio_gerencial()
    {
        $data['titulo'] = 'Estoque';
        $data['gerencialArray'] = $this->M_global->listaEstoque();
        $this->load->view('admin/relatorio_gerencial', $data);
    }

    public function dashboards()
    {
        $data['titulo'] = 'Dash';
        $data['vendasAprovadas'] = $this->M_relatorios->totalVendasAprovadas();

        $data['vendasAprovadasMes'] = $this->M_relatorios->totalVendasAprovadasMes();

        $data['totalClientes'] = $this->M_relatorios->totalClientes();

        $data['faturamentoAnual'] = $this->M_relatorios->totalfaturamentoAnual();
        $data['faturamentoMensal'] = $this->M_relatorios->totalfaturamentoMensal();
        $produtoMaisVendido = $this->M_relatorios->produtosMaisVendidos();

        foreach ($produtoMaisVendido as $produto) {
            $data['totalMaisVendido'][] = ["nome_material" => $produto->nome_material, "saida" => $produto->quantidade_saida];
            $data['totalSaida'][] = ["saida" => $produto->quantidade_saida, "nome" => $produto->nome_material];
        }

        $data['maisVendido'] = max($data['totalSaida']);

        $data['meses'] = array("Janeiro", "Fevereiro",  "Março",  "Abril",  "Maio",  "Junho",  "Julho",  "Agosto",  "Setembro",  "Outubro",  "Novembro", "Dezembro");


        $data['vendasMatriculasMensais'] = [];

        $vendaMatriculasomatorioMesAmes = $this->M_relatorios->vendasMesaMes();

        foreach ($vendaMatriculasomatorioMesAmes as $vendaMatricula) {
            $data['vendasMatriculasMensais'][] = ["total" => $vendaMatricula->total_valido, "data" => $vendaMatricula->data_venda, "mes" => $vendaMatricula->mes];
            $data['total'][] = $vendaMatricula->total_valido;
            $data['mes'][] = $vendaMatricula->data_venda;
        }

        $arrayComportamento = $this->M_relatorios->listaComportamentoDeCompra();

        foreach ($arrayComportamento as $comportamento) {
            $data['totalComportamento'][] = $comportamento->total;
        }



        $this->load->view('admin/dashboards', $data);
    }

    public function pedidos()
    {

        session_start();
        $userId = $_SESSION["user_id"];

        $data['titulo'] = 'Pedidos';
        #$data['menu'] = $this->load->view('menu');
        $data['vendasArray'] = $this->M_global->listaVendasUsuario($userId);

        $this->load->view('meus_pedidos', $data);
    }

    public function historico_pedido($numeroPedido)
    {


        $data['titulo'] = 'Pedidos';
        #$data['menu'] = $this->load->view('menu');
        $data['vendasArray'] = $this->M_global->listahistoricoVenda($numeroPedido);

        $this->load->view('historico_pedido', $data);
    }

    public function contato()
    {


        $data['titulo'] = 'Contato - Jmsports';

        $nome = $_POST["nome"];
        $telefone = $_POST["telefone"];
        $email = $_POST["email"];
        $tipoContato = $_POST["tipo_contato"];
        $numeroPedido = $_POST["numero_pedido"];
        $msg = $_POST["mensagem"];

        $arrayInsert = array(
            "nome" => $nome,
            "telefone" => $telefone,
            "email" => $email,
            "tipo_contato" => $tipoContato,
            "numero_pedido" => $numeroPedido,
            "mensagem" => $msg,
            "data_hora" => date('Y-m-d h:i:s'),
        );

        if ($this->M_global->insertTableMysql("contatos", $arrayInsert)) {
            echo "<script>alert('Obrigado pela mensagem! Em breve entraremos em contato!');</script>";
            echo "<script>window.location.href='../contato';</script>";
        }
    }

    public function atendimento()
    {

        $data['atentimentoContato'] = $this->M_global->listaContatosAtendimento();

        $this->load->view('admin/atendimento', $data);
       
    }

    public function atendendo_contato($idContato)
    {

        $data['atentimentoContato'] = $this->M_global->listaContatosAtendimento($idContato);

        $this->load->view('admin/atendendo_contato', $data);
       
    }
}
