<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('America/Sao_Paulo');

class Carrinho extends CI_Controller
{

    function  __construct()
    {
        parent::__construct();

        // Load product model
        $this->load->model('M_produtos', 'M_global');

        $this->load->helper('url');

        $this->load->library('session');
    }

    public function index()
    {

        $id = $_REQUEST['id'];
        $data['titulo'] = 'Detalhes do Produto';
        #$data['carrinho'] = $this->M_global->getQueryAllRows('carrinho_compras', 'costumer_id', $id);
        $data['produtos'] = $this->M_global->getQueryAllRows('produtos', 'id', $id);
        $produtoObj = $data['produtos'][0];
        $data['imagem'] = $produtoObj->imagem;
        $estoqueArray = $this->M_global->listaEstoqueMateriais($id);


        foreach ($estoqueArray as $estoque) {

            $entrada = $estoque->entrada;
            $saida = $estoque->saida;

            $total = $entrada - $saida;
        }


        $data['quantidade'] = $total;



        $this->load->view('detalhes_produto', $data);
    }

    public function adicionar_produto()
    {
        $proID = $_GET['id'];
        $quantidade = $_POST['quantidade'];
        $tamanho = $_POST['tamanho'];

        // Fetch specific product by ID
        $produto = $this->M_global->getQueryAllRows('produtos', 'id', $proID);

        foreach ($produto as $row) {
            $preco = $row->preco;
            $nome = $row->nome;
            $image = $row->imagem;
        }


        // Add product to the cart
        session_start();

        $userId = $_SESSION["user_id"];

        if (isset($userId)) {
            $data = array(
                'cliente_id' => $userId,
                'quantidade'    => $quantidade,
                'valor_produto'    => $preco * $quantidade,
                'nome_produto'    => $nome,
                'id_produto' => $proID,
                'status_carrinho' => 'Aberto',
                'tamanho' => $tamanho,
                'image' => $image
            );

            if ($this->M_global->insertTableMysql('carrinho_compras', $data)) {
                echo "<script>alert('Produto adicionado ao carrinho!');</script>";
                echo "<script>window.location.href='visualizar_carrinho';</script>";
            }
        } else {
            redirect(base_url(""));
        }
    }

    public function visualizar_carrinho()
    {
        if (!isset($_SESSION["user_id"])) {
            session_start();
        }

        if (isset($_SESSION["user_id"])) {
            $userId = $_SESSION["user_id"];
        } else {
            echo "<script>alert('Faça login para acessar o carrinho de compras!');</script>";
            echo "<script>window.location.href='../';</script>";
        }


        if (isset($userId)) {
            $this->data['titulo'] = "Carrinho";
            $this->data['carrinhoArray'] = $this->M_global->getQueryAllRows('carrinho_compras', 'cliente_id', $userId);
            #$this->data['menu'] = $this->load->view('menu');

            $this->load->view('carrinho_compras', $this->data);
        } else {
            echo "<script>alert('Faça login para acessar o carrinho de compras!');</script>";
            echo "<script>window.location.href='../';</script>";
        }
    }

    public function deletar_item_carrinho()
    {

        $id = $_REQUEST['id_carrinho'];
        if ($this->M_global->deleteTableMysql('carrinho_compras', 'id_carrinho', $id)) {
            echo "<script>alert('Item removido!');</script>";
            echo "<script>window.location.href='visualizar_carrinho';</script>";
        }
    }

    public function finalizar_pedido()
    {
        session_start();

        $userId = $_SESSION["user_id"];

        $this->data['titulo'] = "Carrinho";
        $this->data['carrinhoArray'] = $this->M_global->getQueryAllRows('carrinho_compras', 'cliente_id', $userId);
        $this->data['menu'] = $this->load->view('menu');

        $this->load->view('finalizar_pedido', $this->data);
    }

    public function pagamento()
    {
        session_start();

        $userId = $_SESSION["user_id"];

        $arrayDataEndereco = array(
            'id' => $userId,
            'nome' => $_POST['nome'],
            'cpf' => $_POST['cpf'],
            'email' => $_POST['email'],
            'telefone' => $_POST['telefone'],
            'cep' => $_POST['cep'],
            'estado' => $_POST['estado'],
            'bairro' => $_POST['bairro'],
            'rua' => $_POST['rua'],
            'numero' => $_POST['numero'],
            'referencia' => $_POST['referencia'],
        );

        $carrinhoArray = $this->M_global->getQueryAllRows('carrinho_compras', 'cliente_id', $userId);

        if (!empty($carrinhoArray)) {
            $valor_total = 0;
            foreach ($carrinhoArray as $carrinho) {

                $carrinho->nome_produto;
                $carrinho->quantidade;
                $carrinho->tamanho;
                $carrinho->valor_produto;
                $valor_total += $carrinho->valor_produto;
            }
        }

        $this->data['titulo'] = 'Pagamento';
        $this->data['enderecoEntrega'] = $arrayDataEndereco;
        $this->data['carrinhoArray'] = $carrinhoArray;


        $this->load->view('pagamento', $this->data);
    }

    public function finaliza_pagamento()
    {

        session_start();

        $userId = $_SESSION["user_id"];

        $this->data['titulo'] = 'Finalizado';
        $this->data['dadosComprador'] = $_POST['dadosComprador'];
        $this->data['dadosCarrinho'] = $_POST['dadosCarrinho'];
        $carrinhoObj = json_decode($this->data['dadosCarrinho']);
        $valor_total = $_POST['valorTotal'];
        $nome = $_POST['nomeComprador'];
        $status_compra = null;

        //$sorteio = random_int(0, 1);
        $sorteio = 0;


        if ($sorteio == 0) {
            $status_compra = "Aprovado";
            $labelStatus = "Aprovada";

            foreach ($carrinhoObj as $carrinho) {
                $produtoId = $carrinho->id_produto;
                $quantidade = $carrinho->quantidade;

                //Realizar saida no estoque

                $arrayMovimentacao = array(
                    "saida" => $quantidade,
                    "observacao" => "Venda de produto no ecommerce",
                    "data_hora" => date('Y-m-d h:i:s'),
                    "loja_produto_id" => $produtoId
                );

                $this->M_global->insertTableMysql('estoque_movimentacao', $arrayMovimentacao);
                
            }
        } else {
            $status_compra = "Negado";
            $labelStatus = "Negada";
        }

        $arrayInsert = array(
            'cliente_id' => $userId,
            'numero_pedido'    => date("YmdHis"),
            'valor_venda'    => $valor_total,
            'nome_cliente'    => $nome,
            'status_venda' => $status_compra,
            'data_venda' => date("Y-m-d H:i:s"),
            'forma_pagamento' => $_POST['forma_pagamento']

        );

        $this->M_global->insertTableMysql('vendas', $arrayInsert);

        echo "<script>alert('Compra $labelStatus');</script>";
        echo "<script>window.location.href='../admin/pedidos';</script>";
        //redirect(base_url());

    }
}
