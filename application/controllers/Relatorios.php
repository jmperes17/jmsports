<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Relatorios extends CI_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();
        if (!$this->user->validate_session()) {
            redirect('usuario', 'refresh');
        }

        //   if ($this->user->user_data->permissao_relatorios == 0) {
        //     redirect('home', 'refresh');
        // }

        if ($this->user->user_data->level != 2) {
            redirect('aluno', 'refresh');
        }

        $this->data['userData'] = $this->user->user_data;
        $this->data['CI'] = &get_instance();

        $this->load->model('m_relatorios');

        //print_r($this->user->user_data);die;
        $this->data['css'] = $this->html_lib->get_css();
        $this->data['javascript'] = $this->html_lib->get_javascript();
        $this->data['unidades'] = $this->m_global->getQueryAllOrderBy("unidades", "nome_unidade");
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['menu'] = $this->load->view('menu', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
    }

    public function relatorio_matriculas()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('periodo_id', 'periodo_id', 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->data['periodoAtivoArray'] = $this->m_global->getQuery('periodo_for_winners', 'id_periodo_for_winners', $this->input->post('periodo_id'));
            $this->data['unidadeSelecionada'] = $this->input->post('unidade_selecionada');
        } else {
            // Periodo Ativo 4 winners
            $this->data['periodoAtivoArray'] = $this->m_global->getQuery('periodo_for_winners', 'status', 1);
            $this->data['unidadeSelecionada'] = $this->user->user_data->unidade_logged_id;
        }

        // TODOS OS PERIODOS DO 4WINNERS
        $this->data['periodoArray'] = $this->m_global->getQueryAllOrderBy('periodo_for_winners', 'id_periodo_for_winners', 'ASC');

        // CÁLCULO DE COMPORTAMENTO DE COMPRA
        $listaComportamentoDeCompra = $this->m_relatorios->listaComportamentoDeCompra($this->data['unidadeSelecionada'], $this->data['periodoAtivoArray']->data_inicio, $this->data['periodoAtivoArray']->data_fim);
        $this->data['comportamentoDeCompra'] = [];
        if (!empty($listaComportamentoDeCompra)) {
            $totalCartao = 0;
            $totalBoleto = 0;
            foreach ($listaComportamentoDeCompra as $comportamentoDeCompra) {
                if ($comportamentoDeCompra->forma_pagamento == 1) {
                    $totalCartao = $comportamentoDeCompra->forma_pagamentos;
                } else {
                    $totalBoleto = $comportamentoDeCompra->forma_pagamentos;
                }
            }
            $porcentagemCartao = ($totalCartao * 100) / ($totalBoleto + $totalCartao);
            $porcentagemBoleto = ($totalBoleto * 100) / ($totalBoleto + $totalCartao);
            $this->data['comportamentoDeCompra'][] = ["label" => "Boleto", "y" => $porcentagemBoleto];
            $this->data['comportamentoDeCompra'][] = ["label" => "Cartão", "y" => $porcentagemCartao];
        }
        //-------

        // Cálculo de Livros vendidos
        $this->data['livrosVendidos'] = [];
        $listaVendadeLivros = $this->m_relatorios->analiseVendasLivros($this->data['unidadeSelecionada'], $this->data['periodoAtivoArray']->data_inicio, $this->data['periodoAtivoArray']->data_fim);
        //  $vendadeLivros = $listaVendadeLivros;
        $totalLivros = 0;
        if (!empty($listaVendadeLivros)) {
            // foreach ($vendadeLivros as $livros) {
            //   $totalLivros += $livros->total_material;
            // }
            foreach ($listaVendadeLivros as $listaLivros) {
                //  $porcentagemMaterial = ($listaLivros->total_material * 100) / $totalLivros;
                //  $this->data['livrosVendidos'][] = ["label" => $listaLivros->nome_material, "y" => $porcentagemMaterial];
                $this->data['livrosVendidos'][] = ["label" => $listaLivros->nome_material, "y" => $listaLivros->total_material];
            }
        }
        //-------

        // Cálculo de venda matriculas mês a mês.
        $this->data['vendasMatriculasMensais'] = [];

        $vendaMatriculasomatorioMesAmes = $this->m_relatorios->vendasMatriculaMesaMes($this->data['unidadeSelecionada']);
        if (!empty($vendaMatriculasomatorioMesAmes)) {
            foreach ($vendaMatriculasomatorioMesAmes as $vendaMatricula) {
                $this->data['vendasMatriculasMensais'][] = ["y" => $vendaMatricula->total_valido, "label" => $vendaMatricula->data_venda];
            }
        }
        //--------------

        // Cálculo de Vendas diárias
        $this->data['vendasDiarias'] = [];
        $listaVendasDiarias = $this->m_relatorios->listaVendasDiarias($this->data['unidadeSelecionada'], $this->data['periodoAtivoArray']->data_inicio, $this->data['periodoAtivoArray']->data_fim);
        if (!empty($listaVendasDiarias)) {
            foreach ($listaVendasDiarias as $vendas) {
                $this->data['vendasDiarias'][] = ["y" => $vendas->total, "label" => $vendas->data_venda];
            }
        }
        //-------

        // Cálculo de Matrículas mês a mês.
        $this->data['matriculasMensais'] = [];
        foreach ($this->data['periodoArray'] as $periodo) {
            $somatorioMesAmes = $this->m_relatorios->somatoriomatorioTotalValido($periodo->id_periodo_for_winners, $this->data['unidadeSelecionada']);
            if (!empty($somatorioMesAmes)) {
                $this->data['matriculasMensais'][] = ["y" => $somatorioMesAmes->total_valido, "label" => substr($this->converter_lib->dataMysqlParaDataBrasileira($periodo->data_inicio), 3)];
            }
        }
        //-------

        // Total de vendas de materiais
        $this->data['anoLetivoVigente'] = $this->converter_lib->anoLetivoMatriculaRematricula();
        $this->data['arrayVendasMateriais'] = $this->m_relatorios->totalVendaDeMateriaisDidaticos($this->data['anoLetivoVigente'], 1, null, null, $this->data['unidadeSelecionada']);
        $this->data['total_vendas'] = 0;
        if (!empty($this->data['arrayVendasMateriais'])) {
            foreach ($this->data['arrayVendasMateriais'] as $vendas) {
                $this->data['total_vendas'] += $vendas->total_vendas;
            }
        }

        // Cálculo de matriculas diárias
        $this->data['matriculasDiarias'] = [];
        $listaMatriculasDiarias = $this->m_relatorios->listaMatriculasDiarias($this->data['unidadeSelecionada'], $this->data['periodoAtivoArray']->data_inicio, $this->data['periodoAtivoArray']->data_fim);
        if (!empty($listaMatriculasDiarias)) {
            foreach ($listaMatriculasDiarias as $key => $matriculas) {
                if ($key == 0) {
                    $this->data['matriculasDiarias'][] = ["y" => $matriculas->total_matriculas, "label" => $this->converter_lib->dataMysqlParaDataBrasileira($matriculas->data)];
                } else {
                    $keyAnterior = $key - 1;
                    $dadoAnterior = $listaMatriculasDiarias[$keyAnterior];

                    $totalDiferenca = $matriculas->total_matriculas - $dadoAnterior->total_matriculas;

                    if ($totalDiferenca < 0) {
                        $totalDiferenca = 0;
                    }

                    $this->data['matriculasDiarias'][] = ["y" => $totalDiferenca, "label" => $this->converter_lib->dataMysqlParaDataBrasileira($matriculas->data)];
                }
            }
        }
        //-------


        $this->data['mesVigenteStr'] = $this->converter_lib->mesNumeroParaMesTexto(substr($this->converter_lib->dataMysqlParaDataBrasileira($this->data['periodoAtivoArray']->data_inicio), 3, 2)) . " /" . substr($this->data['periodoAtivoArray']->data_inicio, 0, 4);

        $this->data['listaRelatorioSponteWpensar'] = $this->m_relatorios->listaRelatorioSponteWpensar($this->data['periodoAtivoArray']->id_periodo_for_winners, $this->data['unidadeSelecionada']);
        $this->data['somatorioMatriculasSponte'] = $this->m_relatorios->somatorioMatriculasSponte($this->data['periodoAtivoArray']->id_periodo_for_winners, $this->data['unidadeSelecionada']);
       // $this->data['somatorioMatriculasWpensar'] = $this->m_relatorios->somatorioMatriculasWpensar($this->data['periodoAtivoArray']->id_periodo_for_winners, $this->data['unidadeSelecionada']);
       // $this->data['somatoriomatorioTotalValido'] = $this->m_relatorios->somatoriomatorioTotalValido($this->data['periodoAtivoArray']->id_periodo_for_winners, $this->data['unidadeSelecionada']);

        $this->load->view('relatorios/relatorio_matriculas', $this->data);
    }

    public function relatorio_venda_materiais()
    {
        $this->data['anoLetivoVigente'] = $this->converter_lib->anoLetivoMatriculaRematricula();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipo_venda', 'tipo_venda', 'required');
        if ($this->form_validation->run() == TRUE) {
            $dataInicio = null;
            $dataFim = null;
            $tipoVenda = 1;
            $periodoLetivo = null;
            $this->data['anoLetivoVigente'] = 0;

            if ($this->input->post('ano_letivo')) {
                if ($this->input->post('ano_letivo') != "0") {
                    $periodoLetivo = $this->input->post('ano_letivo');
                    $this->data['anoLetivoVigente'] = $periodoLetivo;
                }
            }
            if ($this->input->post('tipo_venda')) {
                $tipoVenda = $this->input->post('tipo_venda');
            }
            if ($this->input->post('data_inicio')) {
                $dataInicio = $this->converter_lib->dataBrasileiraParaDataMysql($this->input->post('data_inicio'));
            }
            if ($this->input->post('data_fim')) {
                $dataFim = $this->converter_lib->dataBrasileiraParaDataMysql($this->input->post('data_fim'));
            }
            $this->data['arrayVendasMateriais'] = $this->m_relatorios->totalVendaDeMateriaisDidaticos($periodoLetivo, $tipoVenda, $dataInicio, $dataFim);
        } else {
            $this->data['arrayVendasMateriais'] = $this->m_relatorios->totalVendaDeMateriaisDidaticos($this->data['anoLetivoVigente']);
        }

        $this->data['total_vendas'] = 0;
        if (!empty($this->data['arrayVendasMateriais'])) {
            foreach ($this->data['arrayVendasMateriais'] as $vendas) {
                $this->data['total_vendas'] += $vendas->total_vendas;
            }
        }

        // TODOS OS PERIODOS DO 4WINNERS
        $this->data['periodoArray'] = $this->m_global->getQueryAllOrderBy('periodo_for_winners', 'id_periodo_for_winners', 'DESC');

        $this->data['anosLetivosArray'] = $this->m_global->getAnosLetivosVendas();

        $this->load->view('relatorios/relatorio_venda_materiais', $this->data);
    }

    public function relatorio_rematriculas()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('periodo_id', 'periodo_id', 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->data['periodoAtivoArray'] = $this->m_global->getQuery('periodo_for_winners', 'id_periodo_for_winners', $this->input->post('periodo_id'));
            $this->data['unidadeSelecionada'] = $this->input->post('unidade_selecionada');
        } else {
            // Periodo Ativo 4 winners
            $this->data['periodoAtivoArray'] = $this->m_global->getQuery('periodo_for_winners', 'status', 1);
            $this->data['unidadeSelecionada'] = $this->user->user_data->unidade_logged_id;
        }

        // TODOS OS PERIODOS DO 4WINNERS
        $this->data['periodoArray'] = $this->m_global->getQueryAllOrderBy('periodo_for_winners', 'id_periodo_for_winners', 'ASC');

        // CÁLCULO DE COMPORTAMENTO DE COMPRA
        $listaComportamentoDeCompra = $this->m_relatorios->listaComportamentoDeCompra($this->data['unidadeSelecionada'], $this->data['periodoAtivoArray']->data_inicio, $this->data['periodoAtivoArray']->data_fim, 2);
        $this->data['comportamentoDeCompra'] = [];
        if (!empty($listaComportamentoDeCompra)) {
            $totalCartao = 0;
            $totalBoleto = 0;
            foreach ($listaComportamentoDeCompra as $comportamentoDeCompra) {
                if ($comportamentoDeCompra->forma_pagamento == 1) {
                    $totalCartao = $comportamentoDeCompra->forma_pagamentos;
                } else {
                    $totalBoleto = $comportamentoDeCompra->forma_pagamentos;
                }
            }
            $porcentagemCartao = ($totalCartao * 100) / ($totalBoleto + $totalCartao);
            $porcentagemBoleto = ($totalBoleto * 100) / ($totalBoleto + $totalCartao);
            $this->data['comportamentoDeCompra'][] = ["label" => "Boleto", "y" => $porcentagemBoleto];
            $this->data['comportamentoDeCompra'][] = ["label" => "Cartão", "y" => $porcentagemCartao];
        }
        //-------

        // Cálculo de Livros vendidos
        $this->data['livrosVendidos'] = [];
        $listaVendadeLivros = $this->m_relatorios->analiseVendasLivros($this->data['unidadeSelecionada'], $this->data['periodoAtivoArray']->data_inicio, $this->data['periodoAtivoArray']->data_fim, 2);
        //  $vendadeLivros = $listaVendadeLivros;
        $totalLivros = 0;
        if (!empty($listaVendadeLivros)) {
            // foreach ($vendadeLivros as $livros) {
            //   $totalLivros += $livros->total_material;
            // }
            foreach ($listaVendadeLivros as $listaLivros) {
                //  $porcentagemMaterial = ($listaLivros->total_material * 100) / $totalLivros;
                //  $this->data['livrosVendidos'][] = ["label" => $listaLivros->nome_material, "y" => $porcentagemMaterial];
                $this->data['livrosVendidos'][] = ["label" => $listaLivros->nome_material, "y" => $listaLivros->total_material];
            }
        }
        //-------
        // Cálculo de Matrículas mês a mês.
        /*
        $this->data['matriculasMensais'] = [];
        foreach ($this->data['periodoArray'] as $periodo) {
            $somatorioMesAmes = $this->m_relatorios->somatoriomatorioTotalValido($periodo->id_periodo_for_winners, $this->data['unidadeSelecionada']);
            if (!empty($somatorioMesAmes)) {
                $this->data['matriculasMensais'][] = ["y" => $somatorioMesAmes->total_valido, "label" => substr($this->converter_lib->dataMysqlParaDataBrasileira($periodo->data_inicio), 3)];
            }
        }
        */
        //-------

        // Cálculo de venda rematriculas mês a mês.
        $this->data['vendasRematriculasMensais'] = [];

        $vendaRematriculasomatorioMesAmes = $this->m_relatorios->vendasRematriculaMesaMes($this->data['unidadeSelecionada']);
        //   print_r($vendaRematriculasomatorioMesAmes);
        if (!empty($vendaRematriculasomatorioMesAmes)) {
            foreach ($vendaRematriculasomatorioMesAmes as $vendaRematricula) {
                $this->data['vendasRematriculasMensais'][] = ["y" => $vendaRematricula->total_valido, "label" => $vendaRematricula->data_venda];
            }
        }

        //  print_r($this->data['vendasRematriculasMensais']);die;
        //-------

        // Cálculo de Vendas diárias
        $this->data['vendasDiarias'] = [];
        $listaVendasDiarias = $this->m_relatorios->listaVendasDiarias($this->data['unidadeSelecionada'], $this->data['periodoAtivoArray']->data_inicio, $this->data['periodoAtivoArray']->data_fim, 2);
        if (!empty($listaVendasDiarias)) {
            foreach ($listaVendasDiarias as $vendas) {
                $this->data['vendasDiarias'][] = ["y" => $vendas->total, "label" => $vendas->data_venda];
            }
        }
        //-------

        // Cálculo de Vendas diárias
        $this->data['vendasDiariasMesAnterior'] = [];
        if (date("Y") == 1) {
            $mes = 12;
            $ano = date("Y") - 1;
        } else {
            $mes = date("m") - 1;
            $ano = date("Y");
        }

        $listaVendasDiariasMesAnterior = $this->m_relatorios->listaVendasDiariasMesAnterior($this->data['unidadeSelecionada'], $mes, $ano, 2);
        if (!empty($listaVendasDiariasMesAnterior)) {
            foreach ($listaVendasDiariasMesAnterior as $vendas) {
                $this->data['vendasDiariasMesAnterior'][] = ["y" => $vendas->total, "label" => $vendas->data_venda];
            }
        }
        //-------
        // Total de vendas de materiais
        $this->data['anoLetivoVigente'] = $this->converter_lib->anoLetivoMatriculaRematricula();
        $this->data['arrayVendasMateriais'] = $this->m_relatorios->totalVendaDeMateriaisDidaticos($this->data['anoLetivoVigente'], 1, null, null, $this->data['unidadeSelecionada']);
        $this->data['total_vendas'] = 0;
        if (!empty($this->data['arrayVendasMateriais'])) {
            foreach ($this->data['arrayVendasMateriais'] as $vendas) {
                $this->data['total_vendas'] += $vendas->total_vendas;
            }
        }

        // Cálculo de matriculas diárias
        /*
        $this->data['matriculasDiarias'] = [];
        $listaMatriculasDiarias = $this->m_relatorios->listaMatriculasDiarias($this->data['unidadeSelecionada'], $this->data['periodoAtivoArray']->data_inicio, $this->data['periodoAtivoArray']->data_fim);
        if (!empty($listaMatriculasDiarias)) {
            foreach ($listaMatriculasDiarias as $key => $matriculas) {
                if ($key == 0) {
                    $this->data['matriculasDiarias'][] = ["y" => $matriculas->total_matriculas, "label" => $this->converter_lib->dataMysqlParaDataBrasileira($matriculas->data)];
                } else {
                    $keyAnterior = $key - 1;
                    $dadoAnterior = $listaMatriculasDiarias[$keyAnterior];

                    $totalDiferenca = $matriculas->total_matriculas - $dadoAnterior->total_matriculas;

                    if ($totalDiferenca < 0) {
                        $totalDiferenca = 0;
                    }

                    $this->data['matriculasDiarias'][] = ["y" => $totalDiferenca, "label" => $this->converter_lib->dataMysqlParaDataBrasileira($matriculas->data)];
                }
            }
        }
        */
        //-------

        $this->data['mesVigenteStr'] = $this->converter_lib->mesNumeroParaMesTexto(substr($this->converter_lib->dataMysqlParaDataBrasileira($this->data['periodoAtivoArray']->data_inicio), 3, 2)) . " /" . substr($this->data['periodoAtivoArray']->data_inicio, 0, 4);

        //  $this->data['listaRelatorioSponteWpensar'] = $this->m_relatorios->listaRelatorioSponteWpensar($this->data['periodoAtivoArray']->id_periodo_for_winners, $this->data['unidadeSelecionada']);
        //  $this->data['somatorioMatriculasSponte'] = $this->m_relatorios->somatorioMatriculasSponte($this->data['periodoAtivoArray']->id_periodo_for_winners, $this->data['unidadeSelecionada']);
        //  $this->data['somatorioMatriculasWpensar'] = $this->m_relatorios->somatorioMatriculasWpensar($this->data['periodoAtivoArray']->id_periodo_for_winners, $this->data['unidadeSelecionada']);
        //  $this->data['somatoriomatorioTotalValido'] = $this->m_relatorios->somatoriomatorioTotalValido($this->data['periodoAtivoArray']->id_periodo_for_winners, $this->data['unidadeSelecionada']);

        $this->load->view('relatorios/relatorio_rematriculas', $this->data);
    }

    public function matriculas_rematriculas()
    {
        $this->data['periodoArray'] = $this->m_global->getQueryAllOrderBy('periodo_for_winners', 'id_periodo_for_winners', 'DESC');
        $this->data["ano_letivo"] = $this->converter_lib->anoLetivoMatriculaRematricula();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('data_inicio', 'data_inicio', 'required');
        $this->form_validation->set_rules('data_fim', 'data_fim', 'required');

        if ($this->form_validation->run() == TRUE) {

            $dataInicio = $this->converter_lib->dataBrasileiraParaDataMysql($this->input->post('data_inicio'));
            $dataFim = $this->converter_lib->dataBrasileiraParaDataMysql($this->input->post('data_fim'));

            $this->data['data_inicio'] = $this->input->post('data_inicio');
            $this->data['data_fim'] = $this->input->post('data_fim');
            $this->data["filtroInicio"] = $dataInicio;
            $this->data["filtroFim"] = $dataFim;

            $this->data['arrayMatriculasRematriculas'] = $this->m_relatorios->vendasMatriculasRematriculasUnidade($dataInicio, $dataFim);

            if ($this->input->post('periodo_id') > 0) {
                // $this->data['periodoAtivoArray'] = $this->m_global->getQuery('periodo_for_winners', 'id_periodo_for_winners', $this->input->post('periodo_id'));
            }
        } else {
            // Periodo Ativo 4 winners
            $this->data['periodoAtivoArray'] = [];
            $this->data['data_inicio'] = null;
            $this->data['data_fim'] = null;
            $this->data["filtroInicio"] = $this->data['periodoArray'][0]->data_inicio;
            $this->data["filtroFim"] = $this->data['periodoArray'][0]->data_fim;

            $this->data['arrayMatriculasRematriculas'] = $this->m_relatorios->vendasMatriculasRematriculasUnidade($this->data['periodoArray'][0]->data_inicio, $this->data['periodoArray'][0]->data_fim);
        }

        /*
        if (!empty($this->data['periodoAtivoArray'])) {

            $this->data['periodoAtivoArray']->data_inicio;
            $dataArray = explode("-", $this->data['periodoAtivoArray']->data_inicio);

        } else {
        }

        */

        $this->data['totalRematriculas'] = 0;
        $this->data['totalMatriculas'] = 0;
        $this->data['totalExpress'] = 0;
        $this->data['totalPortal'] = 0;
        $this->data['totalSite'] = 0;
        $i = 0;
        if (!empty($this->data['arrayMatriculasRematriculas'])) {
            foreach ($this->data['arrayMatriculasRematriculas'] as $rematricula) {
                $this->data['totalRematriculas'] += $rematricula->total_rematricula;
                $this->data['totalMatriculas'] += $rematricula->total_matricula;
                $this->data['totalExpress'] += $rematricula->total_express;
                $this->data['totalPortal'] += $rematricula->total_matriculas_portal;
                $this->data['totalSite'] += $rematricula->total_matriculas_site;
                $i++;
            }
        }

        $this->data["totalGeralMatriculas"] = $this->data['totalMatriculas'] + $this->data['totalExpress'] + $this->data['totalPortal'] +$this->data['totalSite'];

        // Cálculo de venda matriculas mês a mês.
        $this->data['vendasMatriculasMensais'] = [];

        $i = 0;
        //  $vendaMatriculasomatorioMesAmes = $this->m_relatorios->vendasMatriculaMesaMes();
        $vendaMatriculasomatorioMesAmes = $this->m_relatorios->vendasMatriculasConsolidadas();

        if (!empty($vendaMatriculasomatorioMesAmes)) {
          //  print_r($vendaMatriculasomatorioMesAmes);die;
            foreach ($vendaMatriculasomatorioMesAmes as $vendaMatricula) {
                $this->data['vendasMatriculasMensais'][] = ["label" => $vendaMatricula->data_venda, "y" => $vendaMatricula->total_valido];
                //        if (date('Y') == '2020' && $i == 1) {
                //          $this->data['vendasMatriculasMensais'][] = ["label" => '05/2020', "y" => 0];
                //     }
                $i++;
            }
        }
        //--------------

        // Cálculo de venda rematriculas mês a mês.
        $this->data['vendasRematriculasMensais'] = [];

        /*   if (date('Y') == '2020') {
            $this->data['vendasRematriculasMensais'][] = ["label" => '01/2020', "y" => 0];
            $this->data['vendasRematriculasMensais'][] = ["label" => '02/2020', "y" => 0];
            $this->data['vendasRematriculasMensais'][] = ["label" => '03/2020', "y" => 0];
            $this->data['vendasRematriculasMensais'][] = ["label" => '04/2020', "y" => 0];
        }
*/
        $vendaRematriculasomatorioMesAmes = $this->m_relatorios->vendasRematriculasConsolidadas();

        if (!empty($vendaRematriculasomatorioMesAmes)) {
            foreach ($vendaRematriculasomatorioMesAmes as $vendaRematricula) {
                $this->data['vendasRematriculasMensais'][] = ["label" => $vendaRematricula->data_venda, "y" => $vendaRematricula->total_valido];
            }
        }
        //-------

        // print_r($this->data['vendasRematriculasMensais']);die;

        $this->data['listaVendaEncode'] = base64_encode(json_encode($this->data['arrayMatriculasRematriculas']));

        $this->load->view('relatorios/matriculas_rematriculas', $this->data);
    }


    public function exporta_dados_excel_relatorio_matricula_rematricula()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lista_venda_encode', 'lista_venda_encode', 'required');
        if ($this->form_validation->run() == TRUE) {
            $decode = base64_decode($this->input->post('lista_venda_encode'));
            $this->data['arrayMatriculasRematriculas'] = json_decode($decode);
            $this->load->view('relatorios/exporta_dados_excel_relatorio_matricula_rematricula', $this->data);
        }
    }

    public function matriculas_ativas_sponte()
    {
        $this->data["listaMatriculasSponte"] = $this->m_relatorios->listaMatriculasSponte();

        $this->data["total_turmas_abertas"] = 0;
        $this->data["total_turmas_em_formacao"] = 0;

        if (!empty($this->data["listaMatriculasSponte"])) {
            foreach ($this->data["listaMatriculasSponte"] as $matriculasSponte) {
                $this->data["total_turmas_abertas"] += $matriculasSponte->total_turmas_abertas;
                $this->data["total_turmas_em_formacao"] += $matriculasSponte->total_turmas_em_formacao;
            }
        }
        $this->data["total_matriculas_abertas"] = $this->data["total_turmas_em_formacao"] +  $this->data["total_turmas_abertas"];


        $this->load->view('relatorios/matriculas_ativas_sponte', $this->data);
    }

    public function teste_mongo()
    {
        $this->load->library('mongo_db', array('activate' => 'default'), 'mongo_db');
        $this->data['listaVendaArray'] = $this->mongo_db->order_by(array('_id' => 'DESC'))->limit(1500)->get('venda_materiais_didaticos');
        // $this->data['listaVendaArray'] = $this->mongo_db->find_one('venda_materiais_didaticos');

        $this->load->view('relatorios/teste_mongo', $this->data);
    }

    public function insere_mongo_vendas()
    {
        ini_set('memory_limit', '-1');
        $this->load->library('mongo_db', array('activate' => 'default'), 'mongo_db');

        // $carro = $this->m_global->getQueryAllOrderBy("venda_materiais_didaticos", "id_venda_materiais_didaticos");
        $carro = $this->m_global->getAllWithJoin("venda_materiais_didaticos", "unidades", "unidade_id", "id_unidades", [], 'inner', "id_venda_materiais_didaticos", "ASC", "500");
        foreach ($carro as $carrinho) {
            $arrayVendaMongo = [
                "id_venda_materiais_didaticos" => $carrinho->id_venda_materiais_didaticos,
                "numero_pedido" => $carrinho->numero_pedido,
                "nome_aluno" => $carrinho->nome_aluno,
                "data_nascimento_aluno" => $carrinho->data_nascimento_aluno,
                "cep_aluno" => $carrinho->cep_aluno,
                "rua" => $carrinho->rua,
                "complemento" => $carrinho->complemento,
                "numero" => $carrinho->numero,
                "endereco_aluno" => $carrinho->endereco_aluno,
                "cidade" => $carrinho->cidade,
                "bairro" => $carrinho->bairro,
                "uf" => $carrinho->uf,
                "email_aluno" =>  $carrinho->email_aluno,
                "telefone" =>  $carrinho->telefone,
                "cpf_responsavel" => $carrinho->cpf_responsavel,
                "nome_responsavel" => $carrinho->nome_responsavel,
                "nome_cartao_responsavel" => $carrinho->nome_cartao_responsavel,
                "numero_cartao_responsavel" => $carrinho->nome_cartao_responsavel,
                "status_venda" => $carrinho->status_venda,
                "nome_material" => $carrinho->nome_material,
                "valor_material" => $carrinho->valor_material,
                "valor_parcela" => $carrinho->valor_parcela,
                "valor_frete" => $carrinho->valor_frete,
                "quantidade_parcelas" => $carrinho->quantidade_parcelas,
                "forma_pagamento" => $carrinho->forma_pagamento,
                "data_hora_venda" => $carrinho->data_hora_venda,
                "data_expira_boleto" => $carrinho->data_expira_boleto,
                "data_hora_atualizacao" => $carrinho->data_hora_atualizacao,
                "url_boleto" => $carrinho->url_boleto,
                "plataforma" => $carrinho->plataforma,
                "gateway_pagamento" => $carrinho->gateway_pagamento,
                "unidade_id" => $carrinho->unidade_id,
                "client_id" => $carrinho->client_id,
                "tipo_venda" => $carrinho->tipo_venda,
                "estagio_id" => $carrinho->estagio_id,
                "turma" => $carrinho->turma,
                "turma_id" => $carrinho->turma_id,
                "ano_letivo" => $carrinho->ano_letivo,
                "quantidade_impressoes" => $carrinho->quantidade_impressoes,
                "logistica_recibos_entrega_id" => $carrinho->logistica_recibos_entrega_id,
                "nome_unidade" => $carrinho->nome_unidade,
            ];
            $this->mongo_db->insert('venda_materiais_didaticos', $arrayVendaMongo);
        }
        echo 'Fim';
    }

    public function insere_mongo_historico()
    {
        ini_set('memory_limit', '-1');
        $this->load->library('mongo_db', array('activate' => 'default'), 'mongo_db');

        $venda_materiais_didaticos_historico = $this->m_global->getQueryAllOrderBy("venda_materiais_didaticos_historico", "id_venda_materiais_didaticos_historico");

        foreach ($venda_materiais_didaticos_historico as $historico) {
            $array =  [
                "id_venda_materiais_didaticos_historico" => $historico->id_venda_materiais_didaticos_historico,
                "venda_materiais_didaticos_id" => $historico->venda_materiais_didaticos_id,
                "numero_pedido" => 777777,
                "status" => $historico->status,
                "observacao" => $historico->observacao,
                "url_documento_anexo" => $historico->url_documento_anexo,
                "data_hora_criacao" => $historico->data_hora_criacao,
                "client_id" => $historico->client_id
            ];

            //  $this->mongo_db->where(array('id_venda_materiais_didaticos' => $historico->venda_materiais_didaticos_id))->push(['venda_materiais_didaticos_historico' => $array])->update('venda_materiais_didaticos');
            //  $this->mongo_db->where(array('id_venda_materiais_didaticos'=> $historico->venda_materiais_didaticos_id))->addtoset('venda_materiais_didaticos_historico', $array)->update('venda_materiais_didaticos_historico');
            //  $this->mongo_db->where(array('id_venda_materiais_didaticos'=> $historico->venda_materiais_didaticos_id))->push('venda_materiais_didaticos_historico', $array)->update('venda_materiais_didaticos');
            // $this->mongo_db->where(array('id_venda_materiais_didaticos' => $historico->venda_materiais_didaticos_id))->addtoset('venda_materiais_didaticos_historico', $array)->update('venda_materiais_didaticos');
            echo $historico->venda_materiais_didaticos_id . "<br>";
            //  $this->mongo_db->where(array('id_venda_materiais_didaticos'=>$historico->venda_materiais_didaticos_id))->inc(array('venda_materiais_didaticos_historico' => $array))->update('venda_materiais_didaticos');
            // $this->mongo_db->where(array('id_venda_materiais_didaticos'=> $historico->venda_materiais_didaticos_id))->push('venda_materiais_didaticos_historico', $array)->update('venda_materiais_didaticos');

            $this->mongo_db->where(array('id_venda_materiais_didaticos' => $historico->venda_materiais_didaticos_id))->push(array('venda_materiais_didaticos_historico' => $array))->update('venda_materiais_didaticos');

            $a = $this->mongo_db->where(array('id_venda_materiais_didaticos' => $historico->venda_materiais_didaticos_id))->find_one('venda_materiais_didaticos');

            print_r($a);
            die;
        }
        die;
        $vendaMateriaisMongo = $this->mongo_db->order_by(array('_id' => 'DESC'))->limit(1500)->get('venda_materiais_didaticos');
    }

    public function update_mongo()
    {
        $this->load->library('mongo_db', array('activate' => 'default'), 'mongo_db');

        $this->mongo_db->where(array('id_venda_materiais_didaticos' => '38'))->set('cidade', 'Sao gonça')->update('venda_materiais_didaticos');
        $ab = $this->mongo_db->where(array('id_venda_materiais_didaticos' => '38'))->find_one('venda_materiais_didaticos');

        print_r($ab);
        die;
    }
}
