<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagina extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('url');


	}
	
	public function index() {
		$dados['titulo'] = 'Jm Sports';
		$dados['produtos'] = $this->M_produtos->listaProdutos();
        $this->load->view('home.php', $dados);

	}

	public function sobre() {
		$dados['titulo'] = 'Sobre - Jm Sports';
        $this->load->view('sobre', $dados);

	}

	public function contato() {
		$this->load->helper('form');
		$dados['titulo'] = 'Contato - Jm Sports';
        $this->load->view('contato', $dados);

	}

	public function produtos() {
		$dados['titulo'] = 'Produtos - Jm Sports';
		$dados['produtos'] = $this->M_produtos->listaProdutos();
        $this->load->view('produtos', $dados);
		

	}

	public function login() {
		$dados['titulo'] = 'Login - Jm Sports';
        $this->load->view('login2.html', $dados);

	}
	public function result() {
		$dados['titulo'] = 'Login - Jm Sports';
        $this->load->view('result-contato.php', $dados);

	}
	public function detalhes_produto() {
		$dados['titulo'] = 'Jm Sports';
		$dados['produtos'] = $this->M_produtos->listaProdutos();
        $this->load->view('detalhes_produto', $dados);

	}




}