<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller{
    
    function  __construct(){
        parent::__construct();
        
        // Load product model
        $this->load->model('M_produtos');

        $this->load->helper('url');
    }
    
    function index(){
        $data = array();
        
        // Fetch products from the database
        
        $data['produtos'] = $this->M_produtos->listaProdutos();
        #var_dump($data);
        
        // Load the product list view
        $this->load->view('produtos', $data);
    }

    #function detalhes_produto(){ 
        
        #$dados = array();
        
        #$dados['titulo'] = 'Detalhes do produto';
		#$dados['produtos'] = $this->M_produtos->listaProdutos();
        #$this->load->view('detalhes_produto', $dados);
    #}
    
    function addToCart($proID){

        $proID = $_GET['id'];
        
        // Fetch specific product by ID
        $produto = $this->M_produtos->listaProdutos($proID);

        // Add product to the cart
        $data = array(
            'id'    => $produto['id'],
            'qty'    => 1,
            'price'    => $produto['price'],
            'name'    => $produto['name'],
            'image' => $produto['image']
        );

        #$this->cart->insert($data);
        #$this->cart->insert($data);
        
        // Redirect to the cart page
        
        #$this->load->view('cart/index', $data);
    }


    
    
}



