<html>

<head>
    <title>
        <?= $titulo ?>
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="/assets/img/apple-icon.png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/templatemo.css">
    <link rel="stylesheet" href="/assets/css/custom.css">
    <link rel="stylesheet" href=<?= base_url("assets/css/templatemo.css") ?>>
    <link rel="stylesheet" href=<?= base_url("assets/css/bootstrap.min.css") ?>>
    <link rel="stylesheet" href=<?= base_url("assets/css/custom.css") ?>>
    <link rel="stylesheet" href=<?= base_url("assets/css/templatemo.css") ?>>
    <link rel="apple-touch-icon" href=<?= base_url("assets/img/apple-icon.png") ?>>

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href=<?= base_url("assets/css/fontawesome.min.css") ?>>
    <link rel="stylesheet" type="text/css" href="../assets/css/form-contato.css">
    <!-- -->

    <style>
        #div_pagamento {
            text-align: center;
            margin-left: 30%;
            margin-right: 50%;
        }
    </style>
</head>
<!-- Start Top Nav -->
<nav class="navbar navbar-expand-lg bg-dark navbar-light d-none d-lg-block" id="templatemo_nav_top">
    <div class="container text-light">
        <div class="w-100 d-flex justify-content-between" id="tel">
            <div>
                <i class="fa fa-envelope mx-2"></i>
                <a class="navbar-sm-brand text-light text-decoration-none" href="mailto:contato@jmsports.com">contato@jmsports.com</a>
                <i class="fa fa-phone mx-2"></i>
                <a class="navbar-sm-brand text-light text-decoration-none" href="tel:021-96680-1984">(21)96680-1984</a>
            </div>
            <div>
                <a class="text-light" href="https://fb.com/jmsportstore" target="_blank" rel="sponsored"><i class="fab fa-facebook-f fa-sm fa-fw me-2"></i></a>
                <a class="text-light" href="https://www.instagram.com/jmsportstore" target="_blank"><i class="fab fa-instagram fa-sm fa-fw me-2"></i></a>
            </div>
        </div>
    </div>
</nav>
<!-- Close Top Nav -->

<!-- Header -->
<nav class="navbar navbar-expand-lg navbar-light shadow">
    <div class="container d-flex justify-content-between align-items-center">

        <a class="navbar-brand text-sucess logo h1 align-self-center" href="<?php echo base_url(); ?>">
            Jm Sports
        </a>

        <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
            <div class="flex-fill">
                <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('sobre'); ?>">Sobre</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('produtos'); ?>">Produtos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('contato'); ?>">Contato</a>
                    </li>
                </ul>
            </div>
            <div class="navbar align-self-center d-flex">
                <div class="d-lg-none flex-sm-fill mt-3 mb-4 col-7 col-sm-auto pr-3">
                    <div class="input-group">
                        <input type="text" class="form-control" id="inputMobileSearch" placeholder="Pesquisar ...">
                        <div class="input-group-text">
                            <i class="fa fa-fw fa-search"></i>
                        </div>
                    </div>
                </div>
                <a class="nav-icon d-none d-lg-inline" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">
                    <i class="fa fa-fw fa-search text-dark mr-2"></i>
                </a>
                <a class="nav-icon position-relative text-decoration-none" href="visualizar_carrinho">
                    <i class="fa fa-fw fa-cart-arrow-down text-dark mr-1"></i>

                </a>


                <div class="dropdown">
                    <button type="button" class="btn btn-outline-light dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-fw fa-user text-dark mr-3"></i>
                    </button>
                    <div class="dropdown-menu">
                        <?php
                        if (!isset($_SESSION["user_id"])) {
                            session_start();
                        }

                        if (!isset($_SESSION["logged_in"])) : ?>
                            <a class="dropdown-item" href="<?php echo base_url('admin/entrar') ?>">Entrar</a>
                        <?php else :  ?>
                            <a class="dropdown-item" href="<?php echo base_url('admin/sair') ?>">Sair</a>
                        <?php endif ?>
                        <!--<a class="dropdown-item" href="#">Meus Pedidos</a>-->
                    </div>
                </div>

            </div>
        </div>
    </div>

</nav>

<!-- Close Header -->

<!-- Modal -->
<div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="w-100 pt-1 mb-5 text-right">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="" method="get" class="modal-content modal-body border-0 p-0">
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Pesquisar ...">
                <button type="submit" class="input-group-text bg-success text-light">
                    <i class="fa fa-fw fa-search text-white"></i>
                </button>
            </div>
        </form>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<body>
    <section class="bg-success py-5">
        <div class="col-md-8">
            <p>Dados dos produtos</p><br>
            
            <?php
            $valor_total = 0;
            foreach ($carrinhoArray as $carrinho) {

               echo 'Produto: '. $carrinho->nome_produto . '<br>';
               echo  'Quantidade: '. $carrinho->quantidade . '<br>';
                $carrinho->tamanho;
                $carrinho->valor_produto;
                $valor_total += $carrinho->valor_produto;
            }
            ?>
            Nome: <?= $enderecoEntrega['nome']; ?> <br>
            Endereço de entrega: Rua <?= $enderecoEntrega['rua']; ?> <br>
            Valor Total: <?= $valor_total; ?>
        </div>
    </section>
    <section class="bg-success py-5">
        <div class="col-md-5 text-white" id="div_pagamento">
            <form action="finaliza_pagamento" method="post">
                <input type="hidden" name="dadosComprador" value='<?= json_encode($enderecoEntrega) ?>'>
                <input type="hidden" name="dadosCarrinho" value='<?= json_encode($carrinhoArray) ?>'>
                <input type="hidden" name="valorTotal" value='<?= $valor_total ?>'>
                <input type="hidden" name="nomeComprador" value='<?= $enderecoEntrega['nome'] ?>'>
                
                <div class="field">
                    <label for="nome">Escolha forma de pagamento</label>
                    <input type="radio" class="btn btn-primary" id="pagamento_cartao" name="forma_pagamento" value="2"> Cartão
                    <input type="radio" class="btn btn-success" id="pagamento_boleto" name="forma_pagamento" value="1"> Boleto
                </div>
                <div class="field" id="div_cartao" style="display: none;">
                    <input type="text" class="checkout-input checkout-name" placeholder="Nome no cartão" autofocus>
                    <input type="text" class="checkout-input checkout-exp" placeholder="Mês de vencimento" maxlength="2">
                    <input type="text" class="checkout-input checkout-exp" placeholder="Ano de vencimento" maxlength="2">
                    <input type="text" class="checkout-input checkout-card" placeholder="Número do cartão" maxlength="16">
                    <input type="text" class="checkout-input checkout-cvc" placeholder="CVC" maxlength="3">
                </div>
                <input type="submit" class="btn btn-success" value="Finalizar">
            </form>
        </div>
    </section>

    <script>
        $('input[name=forma_pagamento]').change(function() {
            var value = $('input[name=forma_pagamento]:checked').val();

            if (value == 2) {
                $("#div_cartao").show();
            } else {
                $("#div_boleto").show();
                $("#div_cartao").hide();
            }
        });
    </script>

</body>
<!-- Start Footer -->
<footer class="bg-dark" id="tempaltemo_footer">
    <div class="container">
        <div class="row">

            <div class="col-md-4 pt-5">
                <h2 class="h2 text-light border-bottom pb-3 border-light">Jm Sports Store</h2>
                <ul class="list-unstyled text-light footer-link-list">
                    <li>
                        <i class="fas fa-map-marker-alt fa-fw"></i>
                        Travessa Francisco Ferreira 14
                    </li>
                    <li>
                        <i class="fa fa-phone fa-fw"></i>
                        <a class="text-decoration-none" href="tel:021966801984">(21)966801984</a>
                    </li>
                    <li>
                        <i class="fa fa-envelope fa-fw"></i>
                        <a class="text-decoration-none" href="mailto:contato@jmsports.com">contato@jmsports.com</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-4 pt-5">
                <h2 class="h2 text-light border-bottom pb-3 border-light">Produtos</h2>
                <ul class="list-unstyled text-light footer-link-list">
                    <li><a class="text-decoration-none" href="#">Camisas</a></li>
                    <li><a class="text-decoration-none" href="#">Bolas</a></li>
                    <li><a class="text-decoration-none" href="#">Moda Fitness</a></li>


                </ul>
            </div>

            <div class="col-md-4 pt-5">
                <h2 class="h2 text-light border-bottom pb-3 border-light">Páginas</h2>
                <ul class="list-unstyled text-light footer-link-list">
                    <li><a class="text-decoration-none" href="#">Home</a></li>
                    <li><a class="text-decoration-none" href="#">Sobre</a></li>
                    <li><a class="text-decoration-none" href="#">Produtos</a></li>
                    <li><a class="text-decoration-none" href="#">Contato</a></li>
                </ul>
            </div>

        </div>

        <div class="row text-light mb-4">
            <div class="col-12 mb-3">
                <div class="w-100 my-3 border-top border-light"></div>
            </div>
            <div class="col-auto me-auto">
                <ul class="list-inline text-left footer-icons">
                    <li class="list-inline-item border border-light rounded-circle text-center">
                        <a class="text-light text-decoration-none" target="_blank" href="http://facebook.com/jmsportstore"><i class="fab fa-facebook-f fa-lg fa-fw"></i></a>
                    </li>
                    <li class="list-inline-item border border-light rounded-circle text-center">
                        <a class="text-light text-decoration-none" target="_blank" href="https://www.instagram.com/jmsportstore"><i class="fab fa-instagram fa-lg fa-fw"></i></a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="w-100 bg-black py-3">
        <div class="container">
            <div class="row pt-2">
                <div class="col-12">
                    <p class="text-left text-light">
                        Copyright &copy; 2021 Jm Sports Team
                        | </a>
                    </p>
                </div>
            </div>
        </div>
    </div>

</footer>
<!-- End Footer -->

</html>