<?php $this->load->view('menu');

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-pro.css" />
  <!-- font awesome style -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
  <link rel="stylesheet" href="assets/css/fontawesome.min.css">
  <!-- Custom styles for this template -->
  <link href="assets/css/style-pro.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="assets/css/responsive.css" rel="stylesheet" />
</head>

<body class="sub_page">
  <section class="product_section layout_padding">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          Nossos <span>Produtos</span>
        </h2>
      </div>
      <div class="row">
        <?php foreach ($produtos as $row) { ?>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="box">
              <div class="option_container">
                <div class="options">
                  <a href="<?php echo base_url('Carrinho/index?id=') ?><?php echo $row['id'] ?>" class="option1">
                    Comprar
                  </a>
                </div>
              </div>
              <div class="img-box">
                <img src="<?php echo base_url('assets/uploads/product_images/' . $row['imagem']); ?>" alt="">
              </div>
              <div class="detail-box">
                <h5>
                  <?= $row['nome']; ?>
                </h5>
                <h6>
                  <?= $row['preco'] . ' R$'; ?>
                </h6>
              </div>
            </div>
          </div><!-- col-sm-6 col-md-4 col-lg-3 -->
        <?php } ?>
      </div><!-- row -->
    </div>
  </section>

</body>
<?php
$this->load->view('footer');
?>

</html>