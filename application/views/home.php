<!DOCTYPE html>
<html lang="pt-br">
<?php
$this->load->view("menu");
?>

<!-- Start Banner Hero -->
<div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
    <ol class="carousel-indicators">
        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active"></li>
        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li>
        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="container">
                <div class="row p-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img class="img-fluid" src="assets/img/br.png" alt="">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left align-self-center">
                            <h1 class="h1 text-success"><b>Jm</b> Sports</h1>
                            <h3 class="h2">Brasil Home</h3>
                            <p>
                                A nova Coleção da Seleção Brasileira de 2022, inspirada na onça-pintada, representa a garra incansável que move cada brasileiro.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="container">
                <div class="row p-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img class="img-fluid" src="assets/img/br_black.png" alt="">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left">
                            <h1 class="h1">Brasil Design Edition 2022</h1>
                            <h3 class="h2">Edição Especial!</h3>
                            <p>
                                O tecido ventilado com tecnologia alta qualidade absorve o suor da sua pele e proporciona uma evaporação mais rápida para manter seu corpo seco e confortável.
                                Inspiração em nível profissional
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="container">
                <div class="row p-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img class="img-fluid" src="assets/img/br_blue.png" alt="">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left">
                            <h1 class="h1">Camisa Brasil II</h1>
                            <h3 class="h2">Veste a Garra! </h3>
                            <p>
                                No país que não desiste nunca, garra é a segunda língua.
                                É acreditar até o último segundo.
                                É coletivo. Representa mais de 210 milhões de brasileiros.
                                É a nossa Garra.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Slider -->
    <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
        <i class="fas fa-chevron-left"></i>
    </a>
    <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
        <i class="fas fa-chevron-right"></i>
    </a>
</div>
<!-- End Banner Hero -->


<!-- Start Categories of The Month -->
<section class="container py-5">
    <div class="row text-center pt-3">
        <div class="col-lg-6 m-auto">
            <h1 class="h1">Linha Torcedor</h1>

        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4 p-5 mt-3">
            <a href="#"><img src="assets/uploads/product_images/<?= $produtos[4]['imagem'] ?>" class="rounded-circle img-fluid border"></a>
            <h5 class="text-center mt-3 mb-3"><?= $produtos[4]['nome'] ?></h5>
            <p class="text-center"><a class="btn btn-success" href="<?php echo base_url('Carrinho/index?id=') ?><?= $produtos[4]['id'] ?>" class="option1">
                    Comprar
                </a>
        </div>
        <div class="col-12 col-md-4 p-5 mt-3">
            <a href="#"><img src="assets/uploads/product_images/<?= $produtos[3]['imagem'] ?>" class="rounded-circle img-fluid border"></a>
            <h5 class="text-center mt-3 mb-3"><?= $produtos[3]['nome'] ?></h5>
            <p class="text-center"><a class="btn btn-success" href="<?php echo base_url('Carrinho/index?id=') ?><?= $produtos[3]['id'] ?>" class="option1">
                    Comprar
                </a>
        </div>
        <div class="col-12 col-md-4 p-5 mt-3">
            <a href="#"><img src="assets/uploads/product_images/<?= $produtos[2]['imagem'] ?>" class="rounded-circle img-fluid border"></a>
            <h5 class="text-center mt-3 mb-3"><?= $produtos[2]['nome'] ?></h5>
            <p class="text-center"><a class="btn btn-success" href="<?php echo base_url('Carrinho/index?id=') ?><?= $produtos[2]['id'] ?>" class="option1">
                    Comprar
                </a>
        </div>
    </div>
</section>
<!-- End Categories of The Month -->


<!-- Start Featured Product -->

<!-- End Featured Product -->
<!-- Footer -->
<?php $this->load->view("footer") ?>;
<!-- Footer -->




<!-- Start Script -->
<script src="assets/js/jquery-1.11.0.min.js"></script>
<script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/templatemo.js"></script>
<script src="assets/js/custom.js"></script>
<!-- End Script -->
</body>

</html>