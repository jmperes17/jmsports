<!DOCTYPE html>
<html lang="pt-br">
<?php $this->load->view('menu'); ?>

<head>
    <title>
        <?php echo $titulo ?>
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php base_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php base_url('assets/css/templatemo.css') ?>">
    <link rel="stylesheet" href="<?php base_url('assets/css/custom.css') ?>">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="<?php base_url('assets/css/fontawesome.min.css') ?>">
    <!--
    

-->
</head>


<body>
    <!-- Open Content -->
    <section class="bg-light">
        <div class="container pb-5">
            <div class="row">
                <div class="col-lg-5 mt-5">
                    <div class="card mb-3">
                        <img src="<?php echo base_url('assets/uploads/product_images/' . $imagem); ?>" alt="">
                    </div>
                    <div class="row">
                        <!--Start Controls-->
                        <div class="col-1 align-self-center">
                            <a href="#multi-item-example" role="button" data-bs-slide="prev">
                                <i class="text-dark fas fa-chevron-left"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                        </div>
                        <!--End Controls-->
                        <!--Start Carousel Wrapper-->
                        <div id="multi-item-example" class="col-10 carousel slide carousel-multi-item" data-bs-ride="carousel">
                            <!--Start Slides-->
                            <div class="carousel-inner product-links-wap" role="listbox">

                                <!--First slide-->
                                <div class="carousel-item active">
                                    <div class="row">
                                        <div class="col-4">
                                            <a href="#">
                                                <img class="card-img img-fluid" src="<?php echo base_url('assets/uploads/product_images/' . $imagem); ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="col-4">
                                            <a href="#">
                                                <img class="card-img img-fluid" src="<?php echo base_url('assets/uploads/product_images/' . $imagem); ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="col-4">
                                            <a href="#">
                                                <img class="card-img img-fluid" src="<?php echo base_url('assets/uploads/product_images/' . $imagem); ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!--/.First slide-->

                                <!--Second slide-->
                                <div class="carousel-item">
                                    <div class="row">
                                        <div class="col-4">
                                            <a href="#">
                                                <img class="card-img img-fluid" src="<?php echo base_url('assets/uploads/product_images/' . $imagem); ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="col-4">
                                            <a href="#">
                                                <img class="card-img img-fluid" src="<?php echo base_url('assets/uploads/product_images/' . $imagem); ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="col-4">
                                            <a href="#">
                                                <img class="card-img img-fluid" src="<?php echo base_url('assets/uploads/product_images/' . $imagem); ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!--/.Second slide-->


                            </div>
                            <!--End Slides-->
                        </div>
                        <!--End Carousel Wrapper-->
                        <!--Start Controls-->
                        <div class="col-1 align-self-center">
                            <a href="#multi-item-example" role="button" data-bs-slide="next">
                                <i class="text-dark fas fa-chevron-right"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <!--End Controls-->
                    </div>
                </div>
                <!-- col end -->
                <div class="col-lg-7 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <?php ?>
                            <?php foreach ($produtos as $produto) : ?>
                                <h1 class="h2"><?= $produto->nome ?></h1>

                                <p class="h3 py-2"><?= $produto->preco ?></p>

                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <h6>Brand:</h6>
                                    </li>
                                    <li class="list-inline-item">
                                        <p class="text-muted"><strong><?= $produto->marca ?></strong></p>
                                    </li>
                                </ul>

                                <h6>Descrição</h6>
                                <p><?= $produto->descricao ?></p>
                                <!--<ul class="list-inline">
                                    <li class="list-inline-item">
                                        <h6>Avaliable Color :</h6>
                                    </li>
                                    <li class="list-inline-item">
                                        <p class="text-muted"><strong>White / Black</strong></p>
                                    </li>
                                </ul>-->

                                <form action="adicionar_produto?id=<?php echo $produto->id ?>" method="post">
                                    <div class="row">
                                        <div class="col-auto">
                                            <select name="tamanho" id="tamanho">
                                                <label>Tamanho</label>
                                                </li>
                                                <option name="tamanho" id="tamanho" value="P">P</option>
                                                <option name="tamanho" id="tamanho" value="M">M</option>
                                                <option name="tamanho" id="tamanho" value="G">G</option>
                                            </select>
                                        </div>
                                        <div class="col-auto">
                                            <ul class="list-inline pb-3">
                                                <li class="list-inline-item text-right">
                                                    Quantidade
                                                </li>
                                                <li class="list-inline-item">
                                                    <span class="badge bg-secondary">
                                                        <input type="number" class="qty-input" name="quantidade" id="quantidade" value="1" min="1" max=<?= $quantidade; ?> required>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row pb-3">
                                        <?php if ($quantidade > 0) : ?>
                                            <div class="col d-grid">
                                                <button type="submit" class="btn btn-success btn-lg" name="submit">Adicionar ao carrinho</button>
                                                <!--<a href="adicionar_produto?id=<?php echo $produto->id ?>" class="btn btn-success btn-lg">Adicionar ao carrinho</a>-->
                                            </div>
                                        <?php else : ?>
                                            <div class="col d-grid">
                                                <button type="submit" class="btn btn-danger btn-lg" name="submit">Produto indisponível</button>
                                                <!--<a href="adicionar_produto?id=<?php echo $produto->id ?>" class="btn btn-success btn-lg">Adicionar ao carrinho</a>-->
                                            </div>
                                        <?php endif; ?>
                                    </div>

                                </form>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start Footer -->

    <!-- End Footer -->

    <!-- Start Script -->
    <script src="<?php echo base_url('assets/js/jquery-1.11.0.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/templatemo.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
    <!-- End Script -->

    <!-- Start Slider Script -->
    <script src="<?php echo base_url('assets/js/slick.min.js') ?>"></script>


</body>
<?php
$this->load->view('footer');
?>

</html>