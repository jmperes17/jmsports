<? $menu ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="../assets/css/form-contato.css">
</head>

<body>
    <div class="container-form">
        <div class="child">
            <form class="formulario" action="<?php echo base_url('Carrinho/pagamento'); ?>" method="post">
                <h2>Dados de entrega</h2>

                <div class="field">
                    <label for="nome">Nome completo</label>
                    <input type="text" id="nome" name="nome" placeholder="Digite seu nome*" required>
                </div>

                <div class="field">
                    <label for="nome">CPF</label>
                    <input type="text" id="cpf" name="cpf" placeholder="Digite o CPF*" required>
                </div>

                <div class="field">
                    <label for="telefone">Telefone:</label>
                    <input type="text" id="telefone" name="telefone" placeholder="Digite seu Telefone">
                </div>

                <div class="field">
                    <label for="email">E-Mail:</label>
                    <input type="email" id="email" name="email" placeholder="Digite seu E-Mail*" required>
                </div>

                <div class="field">
                    <label for="mensagem">CEP</label>
                    <input type="text" name="cep" id="cep" placeholder="cep" required></input>
                </div>

                <div class="field">
                    <label for="mensagem">Estado</label>
                    <input type="text" name="estado" id="estado" placeholder="estado" required></input>
                </div>

                <div class="field">
                    <label for="mensagem">Bairro</label>
                    <input type="text" name="bairro" id="bairro" placeholder="bairro" required></input>
                </div>

                <div class="field">
                    <label for="mensagem">Rua</label>
                    <input type="text" name="rua" id="rua" placeholder="rua" required></input>
                </div>

                <div class="field">
                    <label for="mensagem">Número</label>
                    <input type="text" name="numero" id="numero" placeholder="numero" required></input>
                </div>

                <div class="field">
                    <label for="mensagem">Ponto de referência</label>
                    <input type="text" name="referencia" id="referencia" placeholder="Referência" required></input>
                </div>
        </div>
    </div>
    <input type="submit" name="acao" value="Enviar">
    </form>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script>
        $('input[name=forma_pagamento]').change(function() {
            var value = $('input[name=forma_pagamento]:checked').val();

            if (value == 1) {
                $("#div_cartao").show();
            } else {
                $("#div_boleto").show();
                $("#div_cartao").hide();
            }


        });

        $("#telefone").mask("(00) 00000-0000");
        $("#cpf").mask("000.000.000-00");
        $("#cep").mask("00000-000");
    </script>

    <script type="text/javascript">

    </script>

</body>


</html>


<?php $this->load->view('footer'); ?>