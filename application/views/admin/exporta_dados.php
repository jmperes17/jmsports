<table id="tabela_lista_venda_materiais" class="table table-responsive-xl table-striped table-bordered" style="width:100%; font-size: 13px;">
    <thead class="">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Pedido</th>
            <th scope="col">Nome</th>
            <th scope="col">Status</th>
            <th scope="col">Valor</th>
            <th scope="col">Data</th>
            <th scope="col">Forma de Pagamento</th>
            <!--  <th scope="col"></th>  -->
            <!--  <th></th> -->
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($vendasArray)) :
            foreach ($vendasArray as $listaVenda) : 
                $numeroPedido = $listaVenda['numero_pedido']
        ?>
                <tr scope="row">
                    <td><?= $listaVenda['id_venda'] ?></td>
                    <td><?= "'" . $numeroPedido . "'" ?></td>
                    <td><?= $listaVenda['nome_cliente'] ?></td>
                    <td><?= $listaVenda['status_venda'] ?></td>
                    <td> R$ <?= $listaVenda['valor_venda'] ?></td>
                    <td><?= $listaVenda['data_venda'] ?></td>
                    <td><?= $listaVenda['forma_pagamento'] == 1 ? "Boleto" : "CC" ?></td>
                    
                </tr>
            <?php endforeach;
        else : ?>
            <td>Nenhum resultado encontrado.</td>
        <?php endif; ?>
    </tbody>
</table>
<?php

$arquivo = 'dados_venda_jmsports.xls';
// Configurações header para forçar o download
header("Expires: Mon, 26 Jul 2200 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=\"{$arquivo}\"");
header("Content-Description: PHP Generated Data");

?>