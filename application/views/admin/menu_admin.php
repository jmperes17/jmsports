<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
                <div class="brand-logo">
                    <a href="<?= base_url("admin/home") ?>">
                        <img src="<?= base_url("assets/img/jmsportslogo.png") ?>" class="logo-icon" alt="logo icon">
                        <h5 class="logo-text">JmSports</h5>
                    </a>
                </div>

                <ul class="sidebar-menu">
                    <li class="parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Vendas</span></a>
                        <ul class="children">
                            <li><a class="nav-link" href="<?php echo base_url('admin/gerenciar_vendas'); ?>" target="_blank">Gerenciar Vendas</a></li>
                            <li><a class="nav-link" href="<?php echo base_url('admin/exporta_dados'); ?>" target="_blank">Exportar vendas - Excel</a></li>
                        </ul>
                    </li>
                    <li class="parent"><a href="#"><i class="fa fa-dashboard"></i> <span>Estoque</span></a>
                        <ul class="children">
                            <li><a class="nav-link" href="<?php echo base_url('admin/relatorio_estoque'); ?>" target="_blank">Estoque</a></li>
                            <li><a class="nav-link" href="<?php echo base_url('admin/estoque_movimentacao'); ?>" target="_blank">Estoque movimentação</a></li>
                        </ul>
                    </li>
                    <li class="parent"><a href="#"><i class="fa fa-list-ul"></i> <span>Relatórios</span></a>
                        <ul class="children">
                            <li><a class="nav-link" href="<?php echo base_url('admin/dashboards'); ?>" target="_blank">Dashboards</a></li>
                        </ul>
                    </li>
                    <li class="parent"><a href="#"><i class="fa fa-envelope-open"></i> <span>Contato</span></a>
                            <ul class="children">
                                <li><a class="nav-link" href="<?php echo base_url('admin/atendimento'); ?>" target="_blank">Contatos</a></li>
                            </ul>
                        </li>
                    <li class="parent"><a href="#"><i class="fa fa-user-o"></i> <span>Usuários</span></a>
                        <ul class="children">
                            <li><a class="nav-link" href="<?php echo base_url('admin/usuarios'); ?>" target="_blank">Funcionários</a></li>
                            <li><a class="nav-link" href="<?php echo base_url('admin/clientes'); ?>" target="_blank">Clientes</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>