<!DOCTYPE html>
<html lang="pt-br">



<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="<?= base_url("assets/admin/plugins/vectormap/jquery-jvectormap-2.0.2.css") ?>" rel="stylesheet" />
    <!-- simplebar CSS-->
    <link href="<?= base_url("assets/admin/plugins/simplebar/css/simplebar.css") ?>" rel="stylesheet" />
    <!-- perfect scrollbar CSS-->
    <link href="<?= base_url("assets/admin/plugins/perfect-scrollbar/css/perfect-scrollbar.css") ?>" rel="stylesheet" />
    <!-- Bootstrap core CSS-->
    <link href="<?= base_url("assets/admin/css/bootstrap.min.css") ?>" rel="stylesheet" />
    <!-- animate CSS-->
    <link href="<?= base_url("assets/admin/css/animate.css") ?>" rel="stylesheet" type="text/css" />
    <!-- Icons CSS-->
    <link href="<?= base_url("assets/admin/css/icons.css") ?>" rel="stylesheet" type="text/css" />
    <!-- Sidebar CSS-->
    <link href="<?= base_url("assets/admin/css/sidebar-menu.css") ?>" rel="stylesheet" />
    <!-- Custom Style-->
    <link href="<?= base_url("assets/admin/css/app-style.css") ?>" rel="stylesheet" />

    <title>Estoque - Movimentação</title>


</head>

<body>
    <div id="wrapper">
        <?php $this->load->view("admin/menu_admin"); ?>
        <div class="content-wrapper">
            <div class="container-fluid">
                <table class="table table-striped table-bordered" id="example">
                    <thead>
                        <tr>

                            <th scope="col">Nome do produto</th>
                            <th scope="col">Preço</th>
                            <th scope="col">Quantidade</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($estoqueArray as $estoque) : ?>
                            <tr>

                                <td><?= $estoque->nome_material ?></td>
                                <td><?= $estoque->preco ?></td>
                                <td><?= ($estoque->entrada) - ($estoque->saida)  ?></td>
                                <td><?= $estoque->status ?></td>
                            </tr>
                        <?php endforeach; ?>

                    </tbody>

                </table>
                <br>
                <a href="cadastrar_produto" type="submit" class="btn btn-success">Cadastrar produto</a>
            </div>
        </div>
    </div>

    <br>
    <a href="cadastrar_produto" type="submit" class="btn btn-success">Cadastrar produto</a>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
</body>

</html>