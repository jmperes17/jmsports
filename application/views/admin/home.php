<?php

if (empty($_SESSION["user_id"])) {
    session_start();

    if (!isset($_SESSION["logged_in"])) {
        redirect(base_url('admin/index?usuario_nao_logado=true'));
    }
}

$nivel = $_SESSION["level"];

if ($nivel == "3") {
    echo "<script>alert('Sem permissão!');</script>";
    echo "<script>window.location.href='../';</script>";
}

?>
<!doctype html>
<html lang="pt-br">
<link rel="icon" href="admin/assets/images/favicon.ico" type="image/x-icon">
<!-- Vector CSS -->
<link href="<?= base_url("assets/admin/plugins/vectormap/jquery-jvectormap-2.0.2.css") ?>" rel="stylesheet" />
<!-- simplebar CSS-->
<link href="<?= base_url("assets/admin/plugins/simplebar/css/simplebar.css") ?>" rel="stylesheet" />
<!-- perfect scrollbar CSS-->
<link href="<?= base_url("assets/admin/plugins/perfect-scrollbar/css/perfect-scrollbar.css") ?>" rel="stylesheet" />
<!-- Bootstrap core CSS-->
<link href="<?= base_url("assets/admin/css/bootstrap.min.css") ?>" rel="stylesheet" />
<!-- animate CSS-->
<link href="<?= base_url("assets/admin/css/animate.css") ?>" rel="stylesheet" type="text/css" />
<!-- Icons CSS-->
<link href="<?= base_url("assets/admin/css/icons.css") ?>" rel="stylesheet" type="text/css" />
<!-- Sidebar CSS-->
<link href="<?= base_url("assets/admin/css/sidebar-menu.css") ?>" rel="stylesheet" />
<!-- Custom Style-->
<link href="<?= base_url("assets/admin/css/app-style.css") ?>" rel="stylesheet" />


<style>
    .menu {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        background-color: silver;
        border-radius: 50px;
        padding: 50px 80px;
    }

    .body {
        padding: 10px;
        background-size: 100% 100%;
        background-position: center center;
        margin-right: 200px;
        background-attachment: fixed;

        min-height: 100%;
        position: center center;

    }

    .inputs {
        width: 100%;
        margin: 0 auto;
        padding: 20px;
        border: black;
        background: white;
        margin-bottom: 10px;
        border-radius: 10px;
        outline: solid rgb(97, 19, 19) 0.5px;

    }

    #btn_dash {
        padding: 10px;

        width: 100%;
        margin: 0 auto;
        border-radius: 30px;
        border: none;
        text-transform: uppercase;
        font-weight: 500;
        color: #dae1ed;
        background: linear-gradient(to right, rgb(88, 88, 107), rgb(88, 88, 107));
        cursor: pointer;
    }

    #a-dash {
        color: #edebeb;
    }
</style>
<!--Start sidebar-wrapper-->
<?php $this->load->view("admin/menu_admin"); ?>
<nav>
    <div style="margin-left: 40%; margin-top: 20px;">
        <h4>Seja bem-vindo ao Control Panel JmSports</h4>
    </div>

    <div style="margin-left: 40%; margin-top: 20px;">
        <img src="<?= base_url("assets/img/jmsportslogo.png") ?>" alt="logo icon">
    </div>

    <div style="margin-left: 40%; margin-top: 20px;">
        <a class="btn btn-success" href="<?= base_url() ?>" target="_blank">Ir para o site</a>
    </div>
</nav>

<body>

    <!--
    <div>
        <div class="container">
            <div class="menu">
                <h3 style="text-align: center;">Control Panel</h3><br>
                <div class="flex-fill">
                    <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                        <li class="nav-item">
                            <button type="submit" name="submit" class="btn btn-primary" id="btn_dash"><a class="nav-link" href="<?php echo base_url('admin/gerenciar_vendas'); ?>" id="a-dash">Gerenciar Vendas</a></button><br><br>
                        </li>
                        <li class="nav-item">
                            <button type="submit" name="submit" class="btn btn-primary" id="btn_dash"><a class="nav-link" href="<?php echo base_url('admin/dashboards'); ?>" id="a-dash">Dashboards</a></button><br><br>
                        </li>
                        <li class="nav-item">
                            <button type="submit" name="submit" class="btn btn-primary" id="btn_dash"><a class="nav-link" href="<?php echo base_url('admin/exporta_dados'); ?>" id="a-dash">Relatório de vendas</a></button><br><br>
                        </li>
                        <li class="nav-item">
                            <button type="submit" name="submit" class="btn btn-primary" id="btn_dash"><a class="nav-link" href="<?php echo base_url('admin/relatorio_estoque'); ?>" id="a-dash">Relatório de estoque</a></button><br><br>
                        </li>
                        <li class="nav-item">
                            <button type="submit" name="submit" class="btn btn-primary" id="btn_dash"><a class="nav-link" href="<?php echo base_url('admin/dashboards'); ?>" id="a-dash">Relatórios gerenciais</a></button><br><br>
                        </li>
                        <li class="nav-item">
                            <button type="submit" name="submit" class="btn btn-primary" id="btn_dash"><a class="nav-link" href="<?php echo base_url('admin/usuarios'); ?>" id="a-dash">USUÁRIOS DO SISTEMA</a></button><br><br>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>-->
</body>


<script src="<?= base_url("assets/admin/assets/js/sidebar-menu.js") ?>"></script>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url("assets/admin/assets/js/jquery.min.js") ?>"></script>
<script src="<?= base_url("assets/admin/assets/js/popper.min.js") ?>"></script>
<script src="<?= base_url("assets/admin/assets/js/bootstrap.min.js") ?>"></script>

<!-- simplebar js -->
<script src="<?= base_url("assets/admin/assets/plugins/simplebar/js/simplebar.js") ?>"></script>
<!-- perfect scrollbar js -->
<script src="<?= base_url("assets/admin/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js") ?>"></script>
<script>
    new PerfectScrollbar('#scrollbar-container1');
    new PerfectScrollbar('#scrollbar-container2');
</script>
<!-- sidebar-menu js -->
<script src="<?= base_url("assets/admin/assets/js/sidebar-menu.js") ?>"></script>
<!-- Custom scripts -->
<script src="<?= base_url("assets/admin/assets/js/app-script.js") ?>"></script>
<!-- Apex chart -->
<script src="<?= base_url("assets/admin/assets/plugins/apexcharts/apexcharts.js") ?>"></script>
<script src="<?= base_url("assets/admin/assets/js/index.js") ?>"></script>

</script>

</html>
<?php $footer; ?>