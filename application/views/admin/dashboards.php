<!doctype html>
<html lang="pt-br">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-datalabels/2.1.0/chartjs-plugin-datalabels.min.js" integrity="sha512-Tfw6etYMUhL4RTki37niav99C6OHwMDB2iBT5S5piyHO+ltK2YX8Hjy9TXxhE1Gm/TmAV0uaykSpnHKFIAif/A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <title>Dashboards</title>
</head>

<body>
  <div class="card mt-3">
    <div class="card-content">
      <div class="row row-group m-0">
        <div class="col-12 col-lg-4 col-xl-4">
          <div class="card-body">
            <div class="media align-items-center">
              <div class="media-body">
                <p class="mb-0">Total vendas de vendas no ano</p>
                <h5 class="mb-0"><?= $vendasAprovadas[0]->total_vendas ?></h5>
              </div>
              <div class="icon-box float-right rounded-circle bg-inverse-primary">
                <i class="zmdi zmdi-shopping-basket text-primary"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-4 col-xl-4">
          <div class="card-body">
            <div class="media align-items-center">
              <div class="media-body">
                <p class="mb-0">Total de vendas no mês</p>
                <h5 class="mb-0"><?= $vendasAprovadasMes[0]->total_vendas  ?></h5>
              </div>
              <div class="icon-box float-right rounded-circle bg-inverse-success">
                <i class="zmdi zmdi-money text-success"></i>
              </div>
            </div>

          </div>
        </div>
        <div class="col-12 col-lg-4 col-xl-4">
          <div class="card-body">
            <div class="media align-items-center">
              <div class="media-body">
                <p class="mb-0">Total clientes cadastrados</p>
                <h5 class="mb-0"><?= $totalClientes[0]->total_clientes  ?></h5>
              </div>
              <div class="icon-box float-right rounded-circle bg-inverse-danger">
                <i class="zmdi zmdi-accounts-alt text-danger"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr><!-- Faturamento -->
  <div class="card mt-3">
    <div class="card-content">
      <div class="row row-group m-0">
        <div class="col-12 col-lg-4 col-xl-4">
          <div class="card-body">
            <div class="media align-items-center">
              <div class="media-body">
                <p class="mb-0">Faturamento anual</p>
                <h5 class="mb-0">R$ <?= number_format($faturamentoAnual[0]->total_vendas, 2) ?></h5>
              </div>
              <div class="icon-box float-right rounded-circle bg-inverse-primary">
                <i class="zmdi zmdi-shopping-basket text-primary"></i>
              </div>
            </div>

          </div>
        </div>
        <div class="col-12 col-lg-4 col-xl-4">
          <div class="card-body">
            <div class="media align-items-center">
              <div class="media-body">
                <p class="mb-0">Faturamento Mensal</p>
                <h5 class="mb-0">R$ <?= number_format($faturamentoMensal[0]->total_vendas, 2)  ?></h5>
              </div>
              <div class="icon-box float-right rounded-circle bg-inverse-success">
                <i class="zmdi zmdi-money text-success"></i>
              </div>
            </div>

          </div>
        </div>


        <div class="col-12 col-lg-4 col-xl-4">
          <div class="card-body">
            <div class="media align-items-center">
              <div class="media-body">
                <p class="mb-0">Produto com mais saída</p>
                <h5 class="mb-0"><?= $maisVendido['nome']  ?></h5>
              </div>
              <div class="icon-box float-right rounded-circle bg-inverse-danger">
                <i class="zmdi zmdi-accounts-alt text-danger"></i>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="box">
        <div class="col-6 col-lg-6 col-xl-6" style="margin-top: 50px; margin-left:10px;">
          <canvas id="myChart" style="height: 400px; width: 100%;"></canvas>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="box">
        <div class="col-6 col-lg-6 col-xl-6" style="margin-top: 50px">
          <canvas id="myChart2" style="height: 400px; width: 100%;"></canvas>
        </div>
      </div>
    </div>
  </div>




</body>

<script>
  //Histrograma
  const labels = <?php echo json_encode($mes, JSON_NUMERIC_CHECK); ?>;

  const data = {
    labels: labels,
    datasets: [{
      label: 'Histórico de vendas por mês',
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgb(255, 99, 132)',
      borderWidth: 1,
      data: <?php echo json_encode($total, JSON_NUMERIC_CHECK); ?>,
      options: {
        scales: {
          y: {
            beginAtZero: true
          },

        }
      }

    }]
  };

  const config = {
    type: 'bar',
    data: data,
    options: {

    }
  };
</script>


<script>
  //Comportamento
  const data2 = {
    labels: [
      'Boleto',
      'Cartão'
    ],
    datasets: [{
      label: 'Comportamento de compra',
      data: <?php echo json_encode($totalComportamento, JSON_NUMERIC_CHECK); ?>,
      backgroundColor: [
        'rgb(255, 99, 132)',
        'rgb(54, 162, 235)',
        'rgb(255, 205, 86)'
      ],
      color: 'rgb(255, 99, 100)',

      hoverOffset: 4
    }]
  };

  const config2 = {
    type: 'pie',
    data: data2,
    options: {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Comportamento de compra'
        }
      },

    }
  };
</script>

<script>
  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );

  const myChart2 = new Chart(
    document.getElementById('myChart2'),
    config2
  );
</script>




<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>



<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>

</body>


</html>