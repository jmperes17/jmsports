<?php
$this->load->view('menu');

?>
<!DOCTYPE html>
<html lang="pt-br">
</head>

<body>
    <div class="contentpanel">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading div_header_panel" style="min-height: 70px;">

                    <div class="col-lg-12" id="div_em_massa_gerenciador_vendas" style="display: none">
                        <div class="card">
                            <div class="card-header">Ações em Massa</div>
                            <div class="card-body">

                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <select class="form-control form-control-rounded" id="periodo_id" name="periodo_id">

                                            <option>Ações em massa:</option>
                                            <option value="Aprovado">Alterar status para Aprovado</option>
                                            <option value="Conferência do Centro de Distribuição">Alterar status Conferência do Centro de Distribuição</option>
                                            <option value="Em separação">Alterar status para Em separação</option>
                                            <option value="Em conferência">Alterar status para Em conferência</option>
                                            <option value="Pronto para envio">Alterar status para Pronto para envio</option>
                                            <option value="Enviado">Alterar status para Enviado</option>
                                            <option value="Entregue">Alterar status para Entregue</option>
                                            <option value="Devolvido">Alterar status para Devolvido</option>
                                            <option value="Aguardando Aprovação">Alterar status para Aguardando Aprovação</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group pull-right">
                                    <button type="button" class="btn btn-info px-5" id="aplicar_alteracao_em_massa"><i class="icon-lock"></i> Aplicar</button>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">

                        <form method="post" id="form_acoes_em_massa">
                            <input type="hidden" name="justificativa" id="justificativa_hidden">
                            <input type="hidden" name="alteracao" id="alteracao" />
                            <input type="hidden" name="numero_pedido" id="numero_pedido" value="1" />
                            <table id="tabela_lista_venda_materiais" class="table table-responsive-xl table-striped table-bordered" style="width:100%; font-size: 13px;">
                                <thead class="">
                                    <tr>
                                        <th scope="col"><input type="checkbox" id="selectAll" value="">
                                        <th scope="col">Pedido</th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Cliente</th>
                                        <th scope="col">Status venda</th>
                                        <th scope="col">Valor</th>
                                        <th scope="col">Data</th>
                                        <th scope="col">Forma de Pagamento</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($vendasArray)) :
                                        foreach ($vendasArray as $listaVenda) : ?>
                                            <tr scope="row">
                                                <td><input type="checkbox" class="checkbox_venda" name="select_all[]" value="<?= $listaVenda['numero_pedido'] ?>"></td>
                                                <td><?= $listaVenda['numero_pedido'] ?></td>
                                                <td><?= $listaVenda['id_venda'] ?></td>
                                                <td><?= $listaVenda['nome_cliente'] ?></td>
                                                <td><?= $listaVenda['status_venda'] ?></td>
                                                <td> R$ <?= $listaVenda['valor_venda'] ?></td>
                                                <td><?= $listaVenda['data_venda'] ?></td>
                                                <?php if ($listaVenda['forma_pagamento'] == 1) : ?>
                                                    <td><span class="badge badge-pill badge-info">Boleto</span></td>
                                                <?php else : ?>
                                                    <td><span class="badge badge-pill badge-primary">CC</span></td>
                                                <?php endif; ?>
                                                <td><a class="btn btn-warning" href="historico_pedido/<?= $listaVenda['numero_pedido'] ?>">Visualizar</a></td>

                                            </tr>
                                        <?php endforeach;
                                    else : ?>
                                        <td>Nenhum resultado encontrado.</td>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>

    <!-- Start Script -->
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/templatemo.js"></script>
    <script src="assets/js/custom.js"></script>
    <script>
        $("#selectAll").click(function() {
            $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
            if ($(this).is(':checked')) {
                $("#div_em_massa_gerenciador_vendas").show('slow');
                $("#filtro_gerenciar_vendas").hide('slow');

            } else {
                $("#div_em_massa_gerenciador_vendas").hide('slow');
                $("#filtro_gerenciar_vendas").show('slow');
            }
        });

        $("#aplicar_alteracao_em_massa").click(function() {
            $alteracao = $("#alteracao").val();

            // var justificativa = $("#justificativa").val();
            // $("#justificativa_hidden").val(justificativa);

            if ($alteracao == "etiquetas") {
                $('#form_acoes_em_massa').attr('action', base_url + 'admin_vendas/gera_etiquetas');
            } else if ($alteracao == "recibo") {
                $('#form_acoes_em_massa').attr('action', base_url + 'admin_vendas/geraRecibo');
            } else if ($alteracao == "etiquetas_a4") {
                $('#form_acoes_em_massa').attr('action', base_url + 'admin_vendas/gera_etiquetas_a4');
            } else {
                $('#form_acoes_em_massa').attr('action', 'acoesEmMassa');
            }

            $('#form_acoes_em_massa').submit();

        });




        $("#periodo_id").on("change", function() {
            var peridoId = $(this).val();
            $("#alteracao").val(peridoId);

        });


        $(".checkbox_venda").change(function() {
            if ($('.checkbox_venda:checked').length > 0) {
                $("#div_em_massa_gerenciador_vendas").show('slow');
                $("#filtro_gerenciar_vendas").hide('slow');
            } else {
                $("#div_em_massa_gerenciador_vendas").hide('slow');
                $("#filtro_gerenciar_vendas").show('slow');
            }
        });
    </script>
    <!-- End Script -->
</body>
<?php #$this->load->view('footer'); 
?>

</html>