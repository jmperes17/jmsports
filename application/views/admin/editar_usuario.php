<!doctype html>
<html lang="pt">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

  <title>Editar</title>
</head>

<?php



?>



<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12 mt-5">
        <h1 class="text-center">Editar</h1>
        <hr style="height: 1px;color: black;background-color: black;">
      </div>
    </div>
    <?php foreach ($funcionariosArray as $row) : ?>
      <div class="col-md5 mx-auto">
        <form action="update_funcionario?id=<?php echo $row->id ?>" method="post">
          <div class="form-row">
            <label for="">Nome</label>
            <input type="text" name="nome" value="<?php echo $row->nome ?>" class="form-control">
          </div>

          <div class="form-row">
            <label for="">CPF</label>
            <input type="text" name="cpf" id="cpf" value="<?php echo $row->cpf ?>" class="form-control">
          </div>

          <div class="form-row">
            <label for="">Email</label>
            <input name="email" value="<?php echo $row->email ?>" class="form-control"></input>

          </div>

          <div class="form-row">
            <label for="">Telefone</label>
            <input type="text" name="telefone" id="telefone" value="<?php echo $row->telefone ?>" class="form-control">
          </div>
        <?php endforeach; ?>
        <div class="form-row">
          <button type="submit" name="submit" class="btn btn-primary">Salvar</button>
        </div>
        </form>
      </div>

      <!-- Optional JavaScript; choose one of the two! -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>



      <script type="text/javascript">
        $("#telefone").mask("(00) 00000-0000");
        $("#cpf").mask("000.000.000-00");
        $("#cep").mask("00000-000");
      </script>

      <!-- Option 1: Bootstrap Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

      <!-- Option 2: Separate Popper and Bootstrap JS -->
      <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    !-->
</body>

</html>