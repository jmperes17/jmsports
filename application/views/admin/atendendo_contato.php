<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="<?= base_url("assets/admin/plugins/vectormap/jquery-jvectormap-2.0.2.css") ?>" rel="stylesheet" />
    <!-- simplebar CSS-->
    <link href="<?= base_url("assets/admin/plugins/simplebar/css/simplebar.css") ?>" rel="stylesheet" />
    <!-- perfect scrollbar CSS-->
    <link href="<?= base_url("assets/admin/plugins/perfect-scrollbar/css/perfect-scrollbar.css") ?>" rel="stylesheet" />
    <!-- Bootstrap core CSS-->
    <link href="<?= base_url("assets/admin/css/bootstrap.min.css") ?>" rel="stylesheet" />
    <!-- animate CSS-->
    <link href="<?= base_url("assets/admin/css/animate.css") ?>" rel="stylesheet" type="text/css" />
    <!-- Icons CSS-->
    <link href="<?= base_url("assets/admin/css/icons.css") ?>" rel="stylesheet" type="text/css" />
    <!-- Sidebar CSS-->
    <link href="<?= base_url("assets/admin/css/sidebar-menu.css") ?>" rel="stylesheet" />
    <!-- Custom Style-->
    <link href="<?= base_url("assets/admin/css/app-style.css") ?>" rel="stylesheet" />

    <title>Contato - Atendimento</title>
</head>

<body>
    <div id="wrapper">
        <div class="mainwrapper">
            <div class="leftpanel">
                <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
                    <div class="brand-logo">
                        <a href="<?= base_url() ?>">
                            <img src="<?= base_url("assets/img/jmsportslogo.png") ?>" class="logo-icon" alt="logo icon">
                            <h5 class="logo-text">JmSports</h5>
                        </a>
                    </div>

                    <ul class="sidebar-menu">
                        <li class="sidebar-header">Navegação</li>
                        <li class="parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Vendas</span></a>
                            <ul class="children">
                                <li><a class="nav-link" href="<?php echo base_url('admin/gerenciar_vendas'); ?>" target="blank">Gerenciar Vendas</a></li>
                                <li><a class="nav-link" href="<?php echo base_url('admin/exporta_dados'); ?>" target="blank">Exportar vendas - Excel</a></li>
                            </ul>
                        </li>
                        <li class="parent"><a href="#"><i class="fa fa-dashboard"></i> <span>Estoque</span></a>
                            <ul class="children">
                                <li><a class="nav-link" href="<?php echo base_url('admin/relatorio_estoque'); ?>" target="blank">Estoque</a></li>
                                <li><a class="nav-link" href="<?php echo base_url('admin/estoque_movimentacao'); ?>" target="blank">Estoque movimentação</a></li>
                            </ul>
                        </li>
                        <li class="parent"><a href="#"><i class="fa fa-list-ul"></i> <span>Relatórios</span></a>
                            <ul class="children">
                                <li><a class="nav-link" href="<?php echo base_url('admin/dashboards'); ?>" target="blank">Dashboards</a></li>
                            </ul>
                        </li>
                        <li class="parent"><a href="#"><i class="fa fa-list-ul"></i> <span>Contato</span></a>
                            <ul class="children">
                                <li><a class="nav-link" href="<?php echo base_url('admin/atendimento'); ?>" target="blank">Contatos</a></li>
                            </ul>
                        </li>
                        <li class="parent"><a href="#"><i class="fa fa-user-o"></i> <span>Usuários</span></a>
                            <ul class="children">
                                <li><a class="nav-link" href="<?php echo base_url('admin/usuarios'); ?>" target="blank">Usuários do sistema</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="container-fluid">
                <table class="table table-striped table-bordered" id="example">
                    <thead>
                        <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Email</th>
                            <th scope="col">Telefone</th>
                            <th scope="col">Motivo</th>
                            <th scope="col">Data</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($atentimentoContato as $atendimento) : ?>
                            <tr>
                                <td><?= $atendimento->nome ?></td>
                                <td><?= $atendimento->email ?></td>
                                <td><?= $atendimento->telefone ?></td>
                                <?php if ($atendimento->tipo_contato == "1") : ?>
                                    <td>Dúvidas</td>
                                <?php elseif ($atendimento->tipo_contato == "2") : ?>
                                    <td>Elogio/Reclamação</td>
                                <?php else : ?>
                                    <td>Meus pedidos</td>
                                <?php endif ?>
                                <td><?= $atendimento->data_hora ?></td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <?php if ($atendimento->numero_pedido) : ?>
                    <p><label for="">Número do pedido: <?= $atendimento->numero_pedido ?></label></p>
                <?php endif; ?>
                <p><label for="">Mensagem:</label></p><br>
                <div class="media-body">
                    &nbsp;&nbsp;
                    <strong><?= nl2br($atendimento->mensagem) ?></strong>
                    &nbsp;
                </div>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
</body>

</html>