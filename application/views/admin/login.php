<?php
if (!isset($_SESSION)) {
    session_start();

    if (isset($_SESSION["logged_in"])) {
        redirect(base_url(''));
    }
}

?>
<html lang="pt-br">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .container_login {
            text-align: center;
        }

        .inputs {
            width: 70%;
            margin: 0 auto;
            padding: 20px;
            border: black;
            background: silver;
            margin-bottom: 10px;
            border-radius: 50px;
        }

        body {
            background-image: url("../assets/img/background-jm-2.png");
            background-repeat: no-repeat;
            background-size: 100% 100%;
            background-position: center center;
            margin-right: 200px;
            background-attachment: fixed;

            min-height: 100%;
            position: center center;
            width: cover;
        }

        .menu {
            position: absolute;
            left: 65%;
            top: 50%;
            transform: translate(-50%, -50%);
            background-color: white;
            border-radius: 10px;
            padding: 10px 80px;
        }

        .btn {
            padding: 20px;
            width: 50%;
            margin: 0 auto;
            border-radius: 50px;
            border: none;
            text-transform: uppercase;
            font-weight: 700;
            color: none;
            background: linear-gradient(to right, black, black);
            cursor: pointer;
        }
    </style>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <title>Admin</title>
</head>

<body>

    <div class="col-md-12 mt-5">



        <div class="menu">
            <h2 class="text-center" style="color: orangered;background-color: none;">Login</h2>
            <hr style="height: 1px;color: black;background-color: black;">
            <div class="container container_login" id="login_container">

                <form action="<?php echo base_url('Admin/entrar') ?>" method="post">

                    <h4>Bem vindo ao Control Panel Jm Sports</h4>
                    <p>Controle e gestão</p>



                    <input type="text" name="email" placeholder="Usuario" id="email" required class="inputs"><br>
                    <input type="password" name="senha" placeholder="Senha" id="senha" required class="inputs"><br>
                    <button type="submit" name="submit" class="btn btn-primary">Entrar</button><br><br>

                    <a href="cadastrar_usuario" style="color: #0031B2; text-decoration:none;"> <b>Cadastre-se</b></a><br>

                </form>



            </div>


        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

</body>
<?php if (isset($_GET['usuario_invalido'])) : ?>
    <?php echo "<script>alert('Usuário não encontrado');</script>"; ?>
<?php endif; ?>



</html>