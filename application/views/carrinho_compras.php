<?php
$this->load->view('menu');
if (!isset($_SESSION["user_id"])) {
    session_start();
    $userId = $_SESSION["user_id"];
}else{
    $userId = $_SESSION["user_id"];
}

?>

<!DOCTYPE html>
<html lang="pt-br">
<style>
    #tabela {
        text-align: center;
        left: 17%;
        position: absolute;
    }
</style>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="/assets/img/apple-icon.png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/templatemo.css">
    <link rel="stylesheet" href="/assets/css/custom.css">
    <link rel="stylesheet" href=<?= base_url("assets/css/templatemo.css") ?>>
    <link rel="stylesheet" href=<?= base_url("assets/css/bootstrap.min.css") ?>>
    <link rel="stylesheet" href=<?= base_url("assets/css/custom.css") ?>>
    <link rel="stylesheet" href=<?= base_url("assets/css/templatemo.css") ?>>
    <link rel="apple-touch-icon" href=<?= base_url("assets/img/apple-icon.png") ?>>

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href=<?= base_url("assets/css/fontawesome.min.css") ?>>
    <link rel="stylesheet" type="text/css" href="../assets/css/form-contato.css">
    <!-- -->


</head>


<body>
    <section>
        <div class="col-12 col-md-8" id="tabela">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Carrinho de compras</h3>
                    </div>

                    <table id="carrinho" class="table table-hover">
                        <thead class="">

                            <tr scope="row">
                                <th scope="col">Nome do produto</th>
                                <th scope="col">Quantidade</th>
                                <th scope="col">Tamanho</th>
                                <th scope="col">Valor</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($carrinhoArray)) :
                                $valor_total = 0;
                                foreach ($carrinhoArray as $carrinho) : ?>
                                    <tr scope="row">

                                        <td><?= $carrinho->nome_produto ?></td>
                                        <td><?= $carrinho->quantidade ?></td>
                                        <td><?= $carrinho->tamanho ?></td>
                                        <td> R$ <?= $carrinho->valor_produto ?></td>
                                        <?php $valor_total += $carrinho->valor_produto; ?>

                                        <td><a href="deletar_item_carrinho?id_carrinho=<?php echo $carrinho->id_carrinho; ?>" class="btn btn-danger">Remover</a></td>
                                    </tr>
                                <?php endforeach; ?>
                        </tbody>
                    </table>
                    <p>Valor total: R$ <?= $valor_total ?></p>
                    <td><a href="finalizar_pedido?user_id=<?php echo $userId ?>" class="btn btn-warning">Finalizar pedido</a></td>
                <?php else : ?> <td> Nenhum produto no carrinho
                    <td>
                    <?php endif; ?>
                </div>
            </div>
        </div>


    </section>


    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/templatemo.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- End Script -->

</body>
<!-- Start Script -->

</html>