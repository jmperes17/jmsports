<?php $this->load->view('menu'); ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="assets/css/form-contato.css">
</head>

<body>
    <div class="container-form">
        <div class="child">
            <form class="formulario" action="<?php echo base_url('admin/contato'); ?>" method="post">
                <h2>Contato</h2>
                <p> Envie uma mensagem respondendo o formulário abaixo</p>

                <div class="field">
                    <label for="nome">Seu Nome:</label>
                    <input type="text" id="nome" name="nome" placeholder="Digite seu nome*" required>
                </div>

                <div class="field">
                    <label for="telefone">Seu Telefone:</label>
                    <input type="text" id="telefone" name="telefone" placeholder="Digite seu Telefone">
                </div>

                <div class="field">
                    <label for="email">Seu E-Mail:</label>
                    <input type="email" id="email" name="email" placeholder="Digite seu E-Mail*" required>
                </div>

                <div class="field">
                    <label for="Motivo">Motivo do contato</label>
                    <select class="form-select form-select-sm tipo_contato" name="tipo_contato" aria-label=".form-select-sm example" id="pedido">
                        <option selected>Selecione</option>
                        <option value="1">Dúvida</option>
                        <option value="2">Reclamação/Elogio</option>
                        <option value="3">Meus pedidos</option>
                    </select>
                </div>

                <div class="field" id="div_motivo" style="display: none;">
                    <label for="mensagem">Informe o número do pedido em questão</label>
                    <input type="text" id="numero_pedido" name="numero_pedido" placeholder="Número do pedido" >
                </div>

                <div class="field">
                    <label for="mensagem">Sua mensagem:</label>
                    <textarea name="mensagem" id="mensagem" placeholder="Mensagem*" required></textarea>
                </div>

                <input class="btn btn-success" type="submit" value="Enviar">
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#pedido').on('change', function() {
                if (this.value == '3') {
                    $("#div_motivo").show();
                } else {
                    $("#div_motivo").hide();
                }
            });
        });
    </script>
</body>

</html>

<?php $this->load->view('footer'); ?>