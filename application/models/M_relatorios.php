<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_relatorios extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listaRelatorioSponteWpensar($periodoForWinners, $unidadeId = null)
    {
        $this->db->initialize();
        $this->db->select('u.nome_unidade, f.total_matriculas as matriculas_sponte,  w.total_matriculas as matriculas_wpensar, 
                        IF(f.total_matriculas >= w.total_matriculas, f.total_matriculas, w.total_matriculas) as total_valido');
        $this->db->from('for_winners_sponte as f');
        $this->db->join('unidades  u', 'u.sponte_id = f.sponte_id');
        $this->db->join('for_winners  w', 'w.unidade_id = u.id_unidades');
        $this->db->where(['f.periodo_for_winners_id' => $periodoForWinners, ' w.periodo_for_winners_id' => $periodoForWinners]);

        if ($unidadeId && $unidadeId > 1) {
            $this->db->where('u.id_unidades', $unidadeId);
        }

        $this->db->order_by('u.nome_unidade');

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function somatorioMatriculasSponte($periodoForWinners, $unidadeId = null)
    {
        $this->db->initialize();
        $this->db->select('sum(f.total_matriculas) as total_matriculas_sponte');
        $this->db->from('for_winners_sponte as f');
        $this->db->join('unidades  u', 'u.sponte_id = f.sponte_id');
        $this->db->where(['f.periodo_for_winners_id' => $periodoForWinners]);

        if ($unidadeId && $unidadeId > 1) {
            $this->db->where('u.id_unidades', $unidadeId);
        }

        $this->db->order_by('u.nome_unidade');

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result[0];
        }
    }

    public function somatorioMatriculasWpensar($periodoForWinners, $unidadeId = null)
    {
        $this->db->initialize();
        $this->db->select('sum(w.total_matriculas) as total_matriculas_wpensar');
        $this->db->from('for_winners_sponte as f');
        $this->db->join('unidades  u', 'u.sponte_id = f.sponte_id');
        $this->db->where(['f.periodo_for_winners_id' => $periodoForWinners]);

        if ($unidadeId && $unidadeId > 1) {
            $this->db->where('u.id_unidades', $unidadeId);
        }

        $this->db->order_by('u.nome_unidade');

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result[0];
        }
    }

    public function somatoriomatorioTotalValido($periodoForWinners, $unidadeId = null)
    {
        $this->db->initialize();
        $this->db->select("sum(f.total_matriculas) as total_valido, DATE_FORMAT(f.data_atualizacao,'%m/%Y') AS data");
        $this->db->from('for_winners_sponte as f');
        $this->db->join('unidades u', 'u.sponte_id = f.sponte_id');
        // $this->db->join('for_winners  w', 'w.unidade_id = u.id_unidades');

        if ($unidadeId && $unidadeId > 1) {
            $this->db->where('u.id_unidades', $unidadeId);
        }

        // $this->db->where(['f.periodo_for_winners_id' => $periodoForWinners, ' w.periodo_for_winners_id' => $periodoForWinners]);
        $this->db->where(['f.periodo_for_winners_id' => $periodoForWinners]);
        $this->db->order_by('u.nome_unidade');

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result[0];
        }
    }

    public function totalVendaDeMateriaisDidaticos($periodoLetivo = null, $tipoVenda = 1, $dataInicio = null, $dataFim = null, $unidadeId = null)
    {
        $this->db->initialize();
        $this->db->select('count(v.id_venda_materiais_didaticos) as total_vendas, u.nome_unidade, v.ano_letivo, v.tipo_venda');
        $this->db->from('venda_materiais_didaticos as v');
        $this->db->join('unidades as u', 'u.id_unidades = v.unidade_id');
        $this->db->where_in("status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);

        if ($periodoLetivo) {
            $this->db->where('v.ano_letivo', $periodoLetivo);
        }

        if ($tipoVenda) {
            $this->db->where('v.tipo_venda', $tipoVenda);
        }

        if ($unidadeId && $unidadeId > 1) {
            $this->db->where('u.id_unidades', $unidadeId);
        }

        if ($dataInicio && $dataFim) {
            $this->db->where("data_hora_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_hora_venda <=", $dataFim . " 23:59:59");
        } elseif ($dataInicio && !$dataFim) {
            $this->db->where("data_hora_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_hora_venda <=", date("Y-m-d H:i:s"));
        } elseif (!$dataInicio && $dataFim) {
            $this->db->where("data_hora_venda >=", date("Y-m-d H:i:s"));
            $this->db->where("data_hora_venda <=", $dataFim . "23:59:59");
        }
        $this->db->group_by('v.unidade_id');
        $this->db->order_by('u.nome_unidade');

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function totalVendasAprovadas($dataInicio = null, $dataFim = null)
    {
        $this->db->initialize();
        $this->db->select('count(v.id_venda) as total_vendas');
        $this->db->from('vendas as v');
        $this->db->where_in("status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);

        if ($dataInicio && $dataFim) {
            $this->db->where("data_hora_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_hora_venda <=", $dataFim . " 23:59:59");
        } elseif ($dataInicio && !$dataFim) {
            $this->db->where("data_hora_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_hora_venda <=", date("Y-m-d H:i:s"));
        } elseif (!$dataInicio && $dataFim) {
            $this->db->where("data_hora_venda >=", date("Y-m-d H:i:s"));
            $this->db->where("data_hora_venda <=", $dataFim . "23:59:59");
        }


        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function totalfaturamentoAnual($dataInicio = null, $dataFim = null)
    {
        $this->db->initialize();
        $this->db->select('sum(v.valor_venda) as total_vendas');
        $this->db->from('vendas as v');
        $this->db->where_in("status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $dataInicio = date("Y") . "-01-01";
        $dataFim = date("Y") . "-12-31";
        if ($dataInicio && $dataFim) {
            $this->db->where("data_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_venda <=", $dataFim . " 23:59:59");
        } elseif ($dataInicio && !$dataFim) {
            $this->db->where("data_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_venda <=", date("Y-m-d H:i:s"));
        } elseif (!$dataInicio && $dataFim) {
            $this->db->where("data_venda >=", date("Y-m-d H:i:s"));
            $this->db->where("data_venda <=", $dataFim . "23:59:59");
        }


        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function totalVendasAprovadasMes($dataInicio = null, $dataFim = null)
    {
        $this->db->initialize();
        $this->db->select('count(v.id_venda) as total_vendas');
        $this->db->from('vendas as v');
        $this->db->where_in("status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $ano = date("Y");
        $mes = date("m");
        $dataInicio = $ano . "-" . $mes . "-01";
        $dataFim = date("Y-m-d");

        if ($dataInicio && $dataFim) {
            $this->db->where("data_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_venda <=", $dataFim . " 23:59:59");
        } elseif ($dataInicio && !$dataFim) {
            $this->db->where("data_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_venda <=", date("Y-m-d H:i:s"));
        } elseif (!$dataInicio && $dataFim) {
            $this->db->where("data_venda >=", date("Y-m-d H:i:s"));
            $this->db->where("data_venda <=", $dataFim . "23:59:59");
        }


        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function totalfaturamentoMensal($dataInicio = null, $dataFim = null)
    {
        $this->db->initialize();
        $this->db->select('sum(v.valor_venda) as total_vendas');
        $this->db->from('vendas as v');
        $this->db->where_in("status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $ano = date("Y");
        $mes = date("m");
        $dataInicio = $ano . "-" . $mes . "-01";
        $dataFim = date("Y-m-d");

        if ($dataInicio && $dataFim) {
            $this->db->where("data_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_venda <=", $dataFim . " 23:59:59");
        } elseif ($dataInicio && !$dataFim) {
            $this->db->where("data_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_venda <=", date("Y-m-d H:i:s"));
        } elseif (!$dataInicio && $dataFim) {
            $this->db->where("data_venda >=", date("Y-m-d H:i:s"));
            $this->db->where("data_venda <=", $dataFim . "23:59:59");
        }


        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function totalClientes()
    {
        $this->db->initialize();
        $this->db->select('count(v.id) as total_clientes');
        $this->db->from('clientes as v');
        $this->db->where('v.level', 3);

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaComportamentoDeCompra()
    {
        $ano = date("Y");

        $this->db->initialize();
        $this->db->select('count(v.forma_pagamento) as total, forma_pagamento');
        $this->db->from('vendas as v');
        $this->db->where_in("status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $this->db->where(["YEAR (v.data_venda) = $ano"]);


        $this->db->group_by('v.forma_pagamento');
        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function produtosMaisVendidos()
    {
        $this->db->initialize();
        $this->db->select('COUNT(e.nome_material) as total_vendas, sum(em.saida) AS quantidade_saida, e.nome_material, em.loja_produto_id');
        $this->db->from('estoque_movimentacao AS em');
        $this->db->join('estoque_materiais e', 'e.loja_produto_id = em.loja_produto_id');
        $this->db->where('em.observacao', 'Venda de produto no ecommerce');
        $this->db->group_by('e.nome_material');

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaMatriculasDiarias($unidadeId = 1, $dataInicio = null, $dataFim = null)
    {
        $this->db->initialize();
        $this->db->select('data, total_matriculas');
        $this->db->from('total_matriculas_rematriculas');
        $this->db->where('unidade_id', $unidadeId);

        if ($dataInicio && $dataFim) {
            $this->db->where("data >=", $dataInicio);
            $this->db->where("data <=", $dataFim);
        } elseif ($dataInicio && !$dataFim) {
            $this->db->where("data >=", $dataInicio);
            $this->db->where("data <=", date("Y-m-d"));
        } elseif (!$dataInicio && $dataFim) {
            $this->db->where("data >=", date("Y-m-d"));
            $this->db->where("data <=", $dataFim);
        }
        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaVendasDiarias($unidadeId = 1, $dataInicio = null, $dataFim = null,  $tipoVenda = 1)
    {
        $this->db->initialize();
        $this->db->select("count(id_venda_materiais_didaticos) as total, DATE_FORMAT(data_hora_venda,'%d/%m/%Y') AS data_venda");
        $this->db->from('venda_materiais_didaticos');
        $this->db->where_in("status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $this->db->where('tipo_venda', $tipoVenda);

        if ($unidadeId > 1) {
            $this->db->where('unidade_id', $unidadeId);
        }

        if ($dataInicio && $dataFim) {
            $this->db->where("data_hora_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_hora_venda <=", $dataFim . " 23:59:59");
        } elseif ($dataInicio && !$dataFim) {
            $this->db->where("data_hora_venda >=", $dataInicio . " 00:00:00");
            $this->db->where("data_hora_venda <=", date("Y-m-d H:i:s"));
        } elseif (!$dataInicio && $dataFim) {
            $this->db->where("data_hora_venda >=", date("Y-m-d H:i:s"));
            $this->db->where("data_hora_venda <=", $dataFim . "23:59:59");
        }
        $this->db->group_by(array("day(data_hora_venda)", "month(data_hora_venda)", "year(data_hora_venda)"));
        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function totalMatriculasMesAmesSponte($unidadeId = 1)
    {
        $this->db->initialize();
        $ano = date("Y");
        $dataInicio = $ano . "-01-01";
        $dataFim = date("Y-m-d");

        $this->db->select("sum(s.total_matriculas) as total, DATE_FORMAT(s.data_atualizacao,'%m/%Y') AS data, u.id_unidades as unidade_id");
        $this->db->from("for_winners_sponte s");
        $this->db->join('unidades as u', 's.sponte_id = u.sponte_id', 'inner');

        if ($unidadeId > 1) {
            $this->db->where('id_unidades', $unidadeId);
        }

        $this->db->where("data_atualizacao >=", $dataInicio);
        $this->db->where("data_atualizacao <=", $dataFim);
        $this->db->group_by("periodo_for_winners_id");

        $client2 = $this->db->get();
        $result2 = $client2->result();
        $this->db->close();
        return $result2;
    }

    public function totalMatriculasMesAmesWpensar($unidadeId = 1)
    {
        $this->db->initialize();
        $ano = date("Y");
        $dataInicio = $ano . "-01-01";
        $dataFim = date("Y-m-d");

        $this->db->select("count(id_for_winners) as total, DATE_FORMAT(atualizado_em,'%d/%m/%Y') AS data, unidade_id");
        $this->db->from("for_winners");
        if ($unidadeId > 1) {
            $this->db->where('unidade_id', $unidadeId);
        }

        $this->db->where("atualizado_em >=", $dataInicio);
        $this->db->where("atualizado_em <=", $dataFim);
        $client = $this->db->get();

        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result1 = $client->result();
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function vendasRematriculaMesaMes($unidadeId = null)
    {
        $ano = date("Y");
        $this->db->initialize();

        $this->db->select("COUNT(id_venda_materiais_didaticos) as total_valido, DATE_FORMAT(v.data_hora_venda,'%m/%Y') AS data_venda, MONTH(v.data_hora_venda) as mes");
        $this->db->from('venda_materiais_didaticos v');
        $this->db->where_in("v.status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $this->db->where('YEAR(v.data_hora_venda)', $ano);

        if ($unidadeId && $unidadeId > 1) {
            $this->db->where('v.unidade_id', $unidadeId);
        }

        $this->db->where('v.tipo_venda', 2);
        $this->db->group_by("MONTH(v.data_hora_venda)");

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function vendasMatriculaMesaMes($unidadeId = null)
    {
        $ano = date("Y");
        $this->db->initialize();

        $this->db->select("COUNT(id_venda_materiais_didaticos) as total_valido, DATE_FORMAT(v.data_hora_venda,'%m/%Y') AS data_venda, MONTH(v.data_hora_venda) as mes");
        $this->db->from('venda_materiais_didaticos v');
        $this->db->where_in("v.status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $this->db->where('YEAR(v.data_hora_venda)', $ano);

        if ($unidadeId && $unidadeId > 1) {
            $this->db->where('v.unidade_id', $unidadeId);
        }

        $this->db->where('v.tipo_venda', 1);
        $this->db->group_by("MONTH(v.data_hora_venda)");

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function vendasMesaMes()
    {
        $ano = date("Y");
        $this->db->initialize();

        $this->db->select("COUNT(id_venda) as total_valido, DATE_FORMAT(v.data_venda,'%m/%Y') AS data_venda, MONTH(v.data_venda) as mes");
        $this->db->from('vendas v');
        $this->db->where_in("v.status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $this->db->where('YEAR(v.data_venda)', $ano);

        $this->db->group_by("MONTH(v.data_venda)");

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }


    public function vendasMatriculasConsolidadas()
    {
        $this->db->initialize();

        $this->db->select("v.*, v.mes_venda as mes");
        $this->db->from("vendas_matriculas_consolidadas v");

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function vendasRematriculasConsolidadas()
    {
        $this->db->initialize();

        $this->db->select("v.*, v.mes_venda as mes");
        $this->db->from("vendas_rematriculas_consolidadas v");

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaVendasDiariasMesAnterior($unidadeId = 1, $mes, $ano, $tipoVenda = 1)
    {
        $this->db->initialize();
        $this->db->select("COUNT(id_venda_materiais_didaticos) as total, DATE_FORMAT(data_hora_venda,'%d/%m/%Y') AS data_venda ,MONTH(v.data_hora_venda) as mes ");
        $this->db->from('venda_materiais_didaticos v');
        $this->db->where_in("status_venda", ["Aprovado", "Enviado", "Pronto para envio", "Entregue", "Em separação", "Em conferência", "Análise de Pendência", "Conferência do Centro de Distribuição"]);
        $this->db->where('tipo_venda', $tipoVenda);
        $this->db->where('MONTH(v.data_hora_venda)', $mes);
        $this->db->where('YEAR(v.data_hora_venda)', $ano);

        if ($unidadeId > 1) {
            $this->db->where('unidade_id', $unidadeId);
        }
        $this->db->group_by("day(v.data_hora_venda)");

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function vendasMatriculasRematriculasUnidade($inicio = null, $fim = null)
    {
        $dataHoraVenda = "";
        $dataHoraVenda2 = "";
        $dataHoraVenda3 = "";
        $dataHoraVenda4 = "";
        $dataHoraVenda5 = "";

        if ($inicio) {
            // $dataHoraVenda = " and MONTH(ve.data_hora_venda) = $mes and YEAR(ve.data_hora_venda) = $ano ";
            $dataHoraVenda = " and DATE(ve.data_hora_venda) >= '$inicio' and DATE(ve.data_hora_venda) <= '$fim' ";
        }

        if ($inicio) {
            //  $dataHoraVenda2 = " and MONTH(vex.data_hora_venda) = $mes and YEAR(vex.data_hora_venda) = $ano ";
            $dataHoraVenda2 = " and DATE(vex.data_hora_venda) >= '$inicio' and DATE(vex.data_hora_venda) <= '$fim' ";
        }

        if ($inicio) {
            // $dataHoraVenda3 = " and MONTH(vmd.data_hora_venda) = $mes and YEAR(vmd.data_hora_venda) = $ano ";
            $dataHoraVenda3 = " and DATE(vmd.data_hora_venda) >= '$inicio' and DATE(vmd.data_hora_venda) <= '$fim' ";
        }

        if ($inicio) {
            //    $dataHoraVenda4 = " and MONTH(vmdr.data_hora_venda) = $mes and YEAR(vmdr.data_hora_venda) = $ano ";
            $dataHoraVenda4 = " and DATE(vmdr.data_hora_venda) >= '$inicio' and DATE(vmdr.data_hora_venda) <= '$fim' ";
        }

        if ($inicio) {
            // $dataHoraVenda5 = " and MONTH(vm.data_hora_venda) = $mes and YEAR(vm.data_hora_venda) = $ano ";
            $dataHoraVenda5 = " and DATE(vm.data_hora_venda) >= '$inicio' and DATE(vm.data_hora_venda) <= '$fim' ";
        }


        $this->db->initialize();
        $this->db->select("u.id_unidades as unidade_id, u.nome_unidade, 
                        

                            ( 
                                Select count(ve.id_venda_yes_conecta) as total_matriculas 
                                from venda_yes_conecta ve 
                                inner join unidades un on un.id_unidades = ve.unidade_id
                                where ve.status_venda = 'Aprovado' and ve.unidade_id = u.id_unidades $dataHoraVenda
                                GROUP by ve.unidade_id
                                ) as total_matricula,


                            ( 
                                select count(vex.id_venda_materiais_didaticos) 
                                from venda_materiais_didaticos vex 
                                where vex.plataforma = 5 and vex.tipo_venda = 1 and vex.unidade_id = u.id_unidades 
                                and vex.status_venda IN('Aprovado', 'Enviado', 'Pronto para envio', 'Entregue', 'Em separação', 'Em conferência','Análise de Pendência', 'Conferência do Centro de Distribuição') $dataHoraVenda2
                                group by vex.unidade_id 
                            ) as total_express,

                            ( 
                            select count(vmd.id_venda_materiais_didaticos) 
                            from venda_materiais_didaticos vmd 
                            where vmd.plataforma = 3 and vmd.tipo_venda = 1 and vmd.unidade_id = u.id_unidades 
                            and vmd.status_venda IN('Aprovado', 'Enviado', 'Pronto para envio', 'Entregue', 'Em separação', 'Em conferência','Análise de Pendência', 'Conferência do Centro de Distribuição') $dataHoraVenda3
                            group by vmd.unidade_id 
                            ) as total_matriculas_portal,

                            ( 
                            select count(vm.id_venda_materiais_didaticos) 
                            from venda_materiais_didaticos vm 
                            where vm.plataforma = 1 and vm.tipo_venda = 1 and vm.unidade_id = u.id_unidades 
                            and vm.status_venda IN('Aprovado', 'Enviado', 'Pronto para envio', 'Entregue', 'Em separação', 'Em conferência','Análise de Pendência', 'Conferência do Centro de Distribuição') $dataHoraVenda5
                            group by vm.unidade_id 
                            ) as total_matriculas_site,

                            ( 
                            select count(vmdr.id_venda_materiais_didaticos) 
                            from venda_materiais_didaticos vmdr 
                            where vmdr.tipo_venda = 2 and vmdr.unidade_id = u.id_unidades 
                            and vmdr.status_venda IN('Aprovado', 'Enviado', 'Pronto para envio', 'Entregue', 'Em separação', 'Em conferência','Análise de Pendência', 'Conferência do Centro de Distribuição') $dataHoraVenda4
                            group by vmdr.unidade_id 
                            ) as total_rematricula");

        $this->db->from("unidades as u");
        $this->db->where("u.status", 1);

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaMatriculasSponte()
    {
        $this->db->initialize();
        $this->db->select("atualizado_em");
        $this->db->from("historico_matriculas_ativas_sponte");
        $this->db->group_by("atualizado_em");
        $this->db->order_by("atualizado_em DESC");
        $this->db->limit(1);
        $client = $this->db->get();

        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $result1 = $result[0];
            $atualizadoEm = $result1->atualizado_em;
            $this->db->select("h.*, u.nome_unidade as unidade");
            $this->db->from("historico_matriculas_ativas_sponte as h");
            $this->db->join('unidades  u', "u.id_unidades = h.unidade_id");
            $this->db->where(["h.atualizado_em" => $atualizadoEm, "u.status" => 1]);
            $this->db->order_by('u.nome_unidade');

            $client2 = $this->db->get();
            if ($client2->num_rows() == 0) {
                $this->db->close();
                return [];
            } else {
                $this->db->close();
                return $client2->result();
            }
        }
    }
}
