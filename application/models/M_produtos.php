<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_produtos extends CI_Model{
    
    function __construct() {
        $this->proTable = 'produtos';
        $this->custTable = 'clientes';
        $this->ordTable = 'pedidos';
        $this->ordItemsTable = 'itens_pedido';
        $this->vendasTable = 'vendas';
    }
    
    /*
     * Fetch products data from the database
     * @param id returns a single record if specified, otherwise all records
     */
    public function listaProdutos($id = ''){
        $this->db->select('*');
        $this->db->from($this->proTable);
        $this->db->where('status', '1');
        if($id){
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('nome', 'asc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        
        // Return fetched data
        return !empty($result)?$result:false;
    }
    

    
}