<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_global extends CI_Model
{

    function __construct()
    {
        $this->proTable = 'produtos';
        $this->custTable = 'clientes';
        $this->ordTable = 'pedidos';
        $this->ordItemsTable = 'itens_pedido';
        $this->vendasTable = 'vendas';
    }

    /*
     * Fetch products data from the database
     * @param id returns a single record if specified, otherwise all records
     */
    public function listaUsuarios($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->custTable);
        $this->db->where('status', '1');
        $this->db->where_not_in('level', '3');
        if ($id) {
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
        } else {
            $this->db->order_by('nome', 'asc');
            $query = $this->db->get();
            $result = $query->result_array();
        }

        // Return fetched data
        return !empty($result) ? $result : false;
    }

    public function listaClientes($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->custTable);
        $this->db->where('status', '1');
        $this->db->where('level', '3');
        if ($id) {
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
        } else {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            $result = $query->result_array();
        }

        // Return fetched data
        return !empty($result) ? $result : false;
    }

    public function listaUsuariosCadastrados($cpf)
    {
        $this->db->select('*');
        $this->db->from($this->custTable);

        $this->db->where('cpf', $cpf);

        $query = $this->db->get();

        $result = $query->row_array();
        #$result = $query->result_array();


        // Return fetched data
        return !empty($result) ? $result : false;
    }

    public function listaVendas()
    {
        $this->db->select('*');
        $this->db->from('vendas');
        $this->db->order_by('id_venda', 'desc');
        $query = $this->db->get();
        $result = $query->result_array();
        // Return fetched data
        return !empty($result) ? $result : false;
    }

    public function listaVendasUsuario($userId)
    {
        $this->db->select('*');
        $this->db->from('vendas');
        $this->db->order_by('id_venda', 'desc');
        $this->db->where('cliente_id', $userId);
        $query = $this->db->get();
        $result = $query->result_array();
        // Return fetched data
        return !empty($result) ? $result : false;
    }

    public function listahistoricoVenda($numeroPedido)
    {
        $this->db->select('*');
        $this->db->from('vendas_historico');
        $this->db->order_by('id_vendas_historico', 'asc');
        $this->db->where('numero_pedido', $numeroPedido);
        $query = $this->db->get();
        $result = $query->result_array();
        // Return fetched data
        return !empty($result) ? $result : false;
    }

    public function listaEstoque()
    {
        $this->db->select('*');
        $this->db->from('produtos');
        $this->db->order_by('nome', 'desc');
        $query = $this->db->get();
        $result = $query->result_array();
        // Return fetched data
        return !empty($result) ? $result : false;
    }

    public function listaFuncionarios()
    {
        $this->db->initialize();
        $where = array('level' => 2);
        $this->db->select('*');
        $this->db->from('clientes');
        $this->db->where($where);
        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result[0];
        }
    }

    public function getQueryAllRows($table, $whereFlag, $valueFlag)
    {
        $this->db->initialize();
        $where = array($whereFlag => $valueFlag);
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }


    public function getQueryAllWithSelect($table, $select)
    {
        $this->db->initialize();
        $this->db->select("$select");
        $this->db->from($table);
        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function getQueryAllOrderBy($table, $colunOrder, $type = "ASC")
    {
        $this->db->initialize();
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by("$colunOrder $type");
        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function getAllWithJoin($table1, $tableJoin, $table1OnField, $tableJoinOnField, $whereArray = [], $type = 'inner', $orderBy = null, $tyepeOrder = "ASC", $limit = null)
    {
        $this->db->initialize();
        $this->db->select('*');
        $this->db->from("$table1 as a");
        $this->db->join("$tableJoin AS b", "a.$table1OnField = b.$tableJoinOnField", $type);
        if (!empty($whereArray)) {
            $this->db->where($whereArray);
        }

        if ($orderBy) {
            $this->db->order_by("$orderBy $tyepeOrder");
        }
        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $query->result();
            $this->db->close();
            return $result;
        }
    }

    public function updateTableMysqlWhereArray($tabela, $whereArray, $valueWhere, $arrayData)
    {
        $this->db->initialize();
        $this->db->where($whereArray, $valueWhere);
        $this->db->update($tabela, $arrayData);
        $afected = $this->db->affected_rows();
        $this->db->close();
        return ($afected >= 1) ? true : false;
    }

    public function deleteTableMysql($table, $whereColum, $whereData)
    {
        $this->db->initialize();
        $this->db->delete($table, array($whereColum => $whereData));
        $afected = $this->db->affected_rows();
        $this->db->close();
        return ($afected >= 1) ? true : false;
    }

    public function insertTableMysql($table, $arrayData)
    {
        $this->db->initialize();
        $this->db->insert($table, $arrayData);
        $afected = $this->db->affected_rows();
        $this->db->close();
        return ($afected >= 1) ? true : false;
    }

    public function Update($id)
    {
        # Função para realizar alteração dos dados da tabela e depois jogar pra view novamente

        if (
            isset($_POST['nome']) &&
            isset($_POST['sobrenome']) &&
            isset($_POST['cpf'])  &&
            isset($_POST['email']) &&
            isset($_POST['email_2']) &&
            isset($_POST['telefone_1']) &&
            isset($_POST['telefone_2'])
        ) {


            $id = $_REQUEST['id'];

            $data['nome'] = $_POST['nome'];
            $data['sobrenome'] = $_POST['sobrenome'];
            $data['cpf'] = $_POST['cpf'];
            $data['email'] = $_POST['email'];
            $data['email_2'] = $_POST['email_2'];
            $data['telefone_1'] = $_POST['telefone_1'];
            $data['telefone_2'] = $_POST['telefone_2'];

            $queryUpdate = "UPDATE contatos SET nome = '$data[nome]', sobrenome= '$data[sobrenome]', cpf = '$data[cpf]',
             email = '$data[email]', email_2 = '$data[email_2]', telefone_1 = '$data[telefone_1]', telefone_2 = '$data[telefone_2]' WHERE id_contato = '$id'";

            if ($sql = $this->conexao->query($queryUpdate)) {
                echo "<script>alert('Dados alterados com sucesso!');</script>";
                echo "<script>window.location.href='../index.php';</script>";
            }
        }
    }



    public function listaMovimentacaoEstoque($unidadeId = null, $limit = null, $materialDidaticoId = null, $dataInicio = null, $dataFim = null)
    {
        $this->db->initialize();
        $this->db->select('*');
        $this->db->from("estoque_movimentacao as a");
        $this->db->join("estoque_materiais AS b", "a.estoque_material_id = b.id_estoque_materiais", 'inner');
        $this->db->join("unidades AS c", "a.unidade_id = c.id_unidades", 'inner');
        $this->db->join("clients AS d", "a.client_id = d.id_clients", 'left');

        if ($materialDidaticoId) {
            $this->db->where(["a.estoque_material_id" => $materialDidaticoId]);
        }

        if ($unidadeId) {
            $this->db->where(["a.unidade_id" => $unidadeId]);
        }

        if ($dataInicio && $dataFim) {
            $this->db->where("data_hora >=", $dataInicio . " 00:00:00");
            $this->db->where("data_hora <=", $dataFim . " 23:59:59");
        } elseif ($dataInicio && !$dataFim) {
            $this->db->where("data_hora >=", $dataInicio . " 00:00:00");
            $this->db->where("data_hora <=", date("Y-m-d H:i:s"));
        } elseif (!$dataInicio && $dataFim) {
            $this->db->where("data_hora >=", date("Y-m-d H:i:s"));
            $this->db->where("data_hora <=", $dataFim . "23:59:59");
        }

        $this->db->order_by("a.id_estoque_movimentacao DESC");

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $query->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaMovimentacaoEstoqueMaterial($materialDidaticoId)
    {
        $this->db->initialize();
        $this->db->select('*');
        $this->db->from("estoque_movimentacao as a");
        $this->db->join("estoque_materiais AS b", "a.estoque_material_id = b.id_estoque_materiais", 'inner');
        $this->db->join("unidades AS c", "a.unidade_id = c.id_unidades", 'inner');
        $this->db->join("clients AS d", "a.client_id = d.id_clients", 'left');
        $this->db->where(["a.estoque_material_id" => $materialDidaticoId]);
        $this->db->order_by("a.id_estoque_movimentacao DESC");

        $query = $this->db->get();

        if ($query->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $query->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaTotalEstoqueMovimentacao()
    {
        $this->db->initialize();
        $query = $this->db->query("select e.*, 
                                (select sum(entrada) from estoque_movimentacao m where m.loja_produto_id= e.loja_produto_id) - (select sum(saida) from estoque_movimentacao m where m.loja_produto_id = e.loja_produto_id) as total
                                from estoque_materiais as e;");
        $this->db->close();
        return $query->result();
    }

    public function listaEstoqueTotal($id = null)
    {
        $this->db->initialize();
        $this->db->select('*');
        $this->db->from("estoque_movimentacao as a");
        $this->db->join("estoque_materiais AS b", "a.loja_produto_id = b.loja_produto_id", 'inner');
        $this->db->join("produtos AS p", "a.loja_produto_id = p.id", 'inner');
        $this->db->order_by("a.id_estoque_movimentacao DESC");

        if ($id) {
            $this->db->where("a.loja_produto_id", $id);
        }

        $query = $this->db->get();

        if ($query->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $query->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaEstoqueMateriais($id = null)
    {
        $this->db->initialize();
        $this->db->select("m.*, e.loja_produto_id, e.nome_material, e.loja_produto_id,
        (select sum(entrada)  from estoque_movimentacao em where em.loja_produto_id = e.loja_produto_id) as entrada,
        (select sum(saida)  from estoque_movimentacao em where em.loja_produto_id = e.loja_produto_id) as saida");
        $this->db->from("produtos m");
        $this->db->join("estoque_materiais AS e", "e.loja_produto_id= m.id");

        if ($id) {
            $this->db->where("e.loja_produto_id", $id);
        }



        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaEstoqueMovimentacao()
    {
        $this->db->initialize();
        $this->db->select('e.nome_material, em.entrada, em.saida, em.observacao, em.data_hora');
        $this->db->from('estoque_movimentacao AS em');
        $this->db->join('estoque_materiais e', 'e.loja_produto_id = em.loja_produto_id');
        $this->db->order_by('em.id_estoque_movimentacao', 'desc');

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }

    public function listaContatosAtendimento($idContato = null)
    {
        $this->db->initialize();
        $this->db->select('*');
        $this->db->from('contatos');

        if ($idContato) {
            $this->db->where("id_contato", $idContato);
        }

        $client = $this->db->get();
        if ($client->num_rows() == 0) {
            $this->db->close();
            return [];
        } else {
            $result = $client->result();
            $this->db->close();
            return $result;
        }
    }
}
