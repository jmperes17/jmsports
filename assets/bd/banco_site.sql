-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 13-Nov-2022 às 00:36
-- Versão do servidor: 10.4.22-MariaDB
-- versão do PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `banco_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `carrinho_compras`
--

CREATE TABLE `carrinho_compras` (
  `id_carrinho` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `tamanho_produto` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nome_produto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_produto` int(11) NOT NULL,
  `tamanho` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `valor_produto` float(10,2) NOT NULL,
  `status_carrinho` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `carrinho_compras`
--

INSERT INTO `carrinho_compras` (`id_carrinho`, `cliente_id`, `quantidade`, `tamanho_produto`, `nome_produto`, `id_produto`, `tamanho`, `valor_produto`, `status_carrinho`, `image`) VALUES
(61, 1, 2, '', 'teste', 24, 'P', 40.00, 'Aberto', '11a2f7f947617eb85d5141dd26bcb9cd.jpg'),
(64, 2, 1, '', 'Camisa Brasil Centenário', 18, 'P', 270.00, 'Aberto', '5a588f188ade3422e730d324a66d8ce0.jpg'),
(65, 1, 1, '', 'Camisa Brasil Centenário', 18, 'P', 270.00, 'Aberto', '5a588f188ade3422e730d324a66d8ce0.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `criado` datetime NOT NULL,
  `modificado` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive',
  `senha` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2' COMMENT '1= adm | 2= funcionario | 3= cliente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `nome`, `cpf`, `email`, `telefone`, `endereco`, `cep`, `criado`, `modificado`, `status`, `senha`, `level`) VALUES
(1, 'Administrador', '053.364.193-46', 'adm@gmail.com', '(22) 11515-1', 'njbnjnj', '21775530', '2022-06-06 11:50:28', '2022-06-06 11:50:29', '1', '123', '1'),
(2, 'Usuário', '053.364.193-46', 'usuario@gmail.com', '(27) 02314-9', 'Avenida Olegário Maciel', '28220-970', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '123', '3'),
(8, 'Bruno', '146.058.694-84', 'usuario2@gmail.com', '(21) 96657-4464', 'Rua teste', '22621-200', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '123', '2'),
(9, 'Jão', '053.364.193-46', 'teste@gmail.com', '(12) 45644-4456', 'bnfrwb hfbwe', '22621-200', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '123456', '3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque_materiais`
--

CREATE TABLE `estoque_materiais` (
  `id_estoque` int(3) NOT NULL,
  `nome_material` varchar(30) NOT NULL,
  `loja_produto_id` int(3) DEFAULT NULL,
  `tamanho` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `estoque_materiais`
--

INSERT INTO `estoque_materiais` (`id_estoque`, `nome_material`, `loja_produto_id`, `tamanho`) VALUES
(1, 'Camisa Brasil Centenário', 18, NULL),
(2, 'Camisa Brasil Away Feminina', 17, NULL),
(3, 'Camisa Brasil Design Edition', 16, NULL),
(4, 'Camisa Brasil Goleiro', 15, NULL),
(5, 'Camisa do Brasil Away', 14, NULL),
(6, 'Camisa Brasil Home', 13, NULL),
(8, 'teste', 24, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque_movimentacao`
--

CREATE TABLE `estoque_movimentacao` (
  `id_estoque_movimentacao` int(3) NOT NULL,
  `entrada` int(11) DEFAULT NULL,
  `saida` int(11) DEFAULT NULL,
  `observacao` text DEFAULT NULL,
  `data_hora` datetime DEFAULT NULL,
  `loja_produto_id` int(3) DEFAULT NULL,
  `vendas_id` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `estoque_movimentacao`
--

INSERT INTO `estoque_movimentacao` (`id_estoque_movimentacao`, `entrada`, `saida`, `observacao`, `data_hora`, `loja_produto_id`, `vendas_id`) VALUES
(1, 200, 0, 'Entrada manual', NULL, 18, NULL),
(2, 200, 0, 'Entrada manual', NULL, 17, NULL),
(3, 200, 0, 'Entrada manual', NULL, 16, NULL),
(4, 200, 0, 'Entrada manual', NULL, 15, NULL),
(5, 200, 0, 'Entrada manual', NULL, 14, NULL),
(6, 200, 0, 'Entrada manual', NULL, 13, NULL),
(8, 20, 0, 'Entrada manual', '2022-11-05 09:32:44', 24, NULL),
(11, NULL, 2, 'Venda de produto no ecommerce', '2022-11-06 12:16:24', 15, NULL),
(12, NULL, 2, 'Venda de produto no ecommerce', '2022-11-06 12:16:41', 24, NULL),
(13, NULL, 2, 'Venda de produto no ecommerce', '2022-11-06 12:21:24', 24, NULL),
(14, NULL, 1, 'Venda de produto no ecommerce', '2022-11-06 12:21:24', 24, NULL),
(15, NULL, 50, 'Venda de produto no ecommerce', '2022-11-06 12:21:24', 17, NULL),
(16, NULL, 2, 'Venda de produto no ecommerce', '2022-11-07 02:09:05', 24, NULL),
(17, NULL, 2, 'Venda de produto no ecommerce', '2022-11-12 04:28:14', 24, NULL),
(18, NULL, 1, 'Venda de produto no ecommerce', '2022-11-12 04:28:14', 18, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens_pedidos`
--

CREATE TABLE `itens_pedidos` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `quantidade` int(5) NOT NULL,
  `sub_total` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `valor_total` float(10,2) NOT NULL,
  `criado` datetime NOT NULL,
  `modificado` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `preco` float(10,2) NOT NULL,
  `marca` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `tamanho` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  `criado` datetime NOT NULL,
  `modificado` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `imagem`, `nome`, `descricao`, `preco`, `marca`, `tamanho`, `criado`, `modificado`, `status`) VALUES
(13, '97eb1bf792273ff22341bd6291c16b51.jpeg', 'Camisa Brasil Home', 'Camisa Brasil I', 270.00, 'NIKE', 'M', '2022-10-23 07:12:03', '2022-10-23 07:12:03', '1'),
(14, 'd32f2a91a1f1a3bc6697147798656c23.jpeg', 'Camisa do Brasil Away', 'Camisa do Brasil II', 270.00, 'NIKE', 'M', '2022-10-23 07:15:00', '2022-10-23 07:15:00', '1'),
(15, 'fb27eb96b805612ba57b1b27d953a460.jpeg', 'Camisa Brasil Goleiro', 'Camisa do Brasil Goleiro', 270.00, 'NIKE', 'G', '2022-10-23 07:15:42', '2022-10-23 07:15:42', '1'),
(16, '27b0514630e78755d15f064741e4a58b.jpeg', 'Camisa Brasil Design Edition', 'Camisa Brasil Design Edition', 300.00, 'NIKE', 'GG', '2022-10-23 07:16:12', '2022-10-23 07:16:12', '1'),
(17, 'c2af233b3701114f278f21dcd97a48f5.jpeg', 'Camisa Brasil Away Feminina', 'Camisa Brasil Away Feminina', 270.00, 'NIKE', 'M', '2022-10-23 07:16:55', '2022-10-23 07:16:55', '1'),
(18, '5a588f188ade3422e730d324a66d8ce0.jpg', 'Camisa Brasil Centenário', 'Camisa Brasil Centenário', 270.00, 'NIKE', 'G', '2022-10-23 07:17:46', '2022-10-23 07:17:46', '1'),
(24, '11a2f7f947617eb85d5141dd26bcb9cd.jpg', 'teste', 'teste', 20.00, 'puma', 'M', '2022-11-05 09:32:44', '2022-11-05 09:32:44', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas`
--

CREATE TABLE `vendas` (
  `id_venda` int(11) NOT NULL,
  `numero_pedido` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `cliente_id` int(11) NOT NULL,
  `nome_cliente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor_venda` float(10,2) NOT NULL,
  `status_venda` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_venda` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forma_pagamento` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vendas`
--

INSERT INTO `vendas` (`id_venda`, `numero_pedido`, `cliente_id`, `nome_cliente`, `valor_venda`, `status_venda`, `data_venda`, `forma_pagamento`) VALUES
(6, '20221105200113', 1, 'Administrador 2', 270.00, 'Aprovado', '2022-11-05 20:01:13', 1),
(7, '20221105200346', 1, 'Jão', 270.00, 'Aprovado', '2022-05-05 20:01:13', 1),
(8, '20221105201521', 1, 'Jão', 270.00, 'Negado', '2022-11-05 20:01:13', 1),
(9, '20221105235405', 1, 'Jão', 810.00, 'Negado', '2022-11-05 20:01:13', 1),
(10, '20221106000134', 1, 'Ciclano', 540.00, 'Aprovado', '2022-04-05 20:01:13', 1),
(11, '20221106000308', 1, 'Jão', 1080.00, 'Negado', '2022-11-05 20:01:13', 1),
(12, '20221106001646', 1, 'Administrador 2', 580.00, 'Aprovado', '2022-03-05 20:01:13', 1),
(13, '20221106002124', 1, 'Administrador 2', 13560.00, 'Aprovado', '2022-02-05 20:01:13', 1),
(14, '20221107140905', 1, 'teste unidade', 40.00, 'Aprovado', '2022-01-05 20:01:13', 1),
(15, '20221108185319', 2, 'testando', 270.00, 'Negado', '2022-11-05 20:01:13', 1),
(16, '20221108185502', 2, 'testando', 270.00, 'Aprovado', '2022-10-05 20:01:13', 1),
(17, '20221112162814', 1, 'Jão', 310.00, 'Entregue', '2022-11-12 16:28:14', 1),
(18, '20221112162815', 1, 'Jão', 310.00, 'Entregue', '2022-06-12 16:28:14', 1),
(19, '20221112162816', 1, 'Jão', 310.00, 'Entregue', '2022-06-12 16:28:14', 1),
(20, '20221112162818', 1, 'Jão', 310.00, 'Entregue', '2022-07-12 16:28:14', 1),
(21, '20221112162819', 1, 'Jão', 310.00, 'Entregue', '2022-11-12 16:28:14', 1),
(22, '20221112162820', 1, 'Jão', 310.00, 'Entregue', '2022-08-12 16:28:14', 1),
(23, '20221112162821', 1, 'Jão', 310.00, 'Entregue', '2022-09-12 16:28:14', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_historico`
--

CREATE TABLE `vendas_historico` (
  `id_vendas_historico` int(11) NOT NULL,
  `id_vendas` int(11) DEFAULT NULL,
  `numero_pedido` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_id` int(11) NOT NULL,
  `observacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_hora_criacao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `vendas_historico`
--

INSERT INTO `vendas_historico` (`id_vendas_historico`, `id_vendas`, `numero_pedido`, `cliente_id`, `observacao`, `status`, `data_hora_criacao`) VALUES
(15, 16, '20221108185502', 2, 'Aprovado', '', '2022-11-08 22:53:11'),
(16, 16, '20221108185502', 2, 'Enviado', '', '2022-11-08 22:53:16'),
(17, NULL, '20221108185502', 1, 'Em separação', '', '2022-11-09 03:06:02'),
(18, NULL, '20221108185502', 1, 'Entregue', '', '2022-11-09 03:14:42'),
(19, NULL, '20221112162814', 1, 'Enviado', '', '2022-11-12 20:35:32'),
(20, NULL, '20221112162814', 1, 'Entregue', '', '2022-11-12 16:36:42');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `carrinho_compras`
--
ALTER TABLE `carrinho_compras`
  ADD PRIMARY KEY (`id_carrinho`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Índices para tabela `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `estoque_materiais`
--
ALTER TABLE `estoque_materiais`
  ADD PRIMARY KEY (`id_estoque`);

--
-- Índices para tabela `estoque_movimentacao`
--
ALTER TABLE `estoque_movimentacao`
  ADD PRIMARY KEY (`id_estoque_movimentacao`);

--
-- Índices para tabela `itens_pedidos`
--
ALTER TABLE `itens_pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Índices para tabela `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `vendas`
--
ALTER TABLE `vendas`
  ADD PRIMARY KEY (`id_venda`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Índices para tabela `vendas_historico`
--
ALTER TABLE `vendas_historico`
  ADD PRIMARY KEY (`id_vendas_historico`) USING BTREE,
  ADD KEY `cliente_id` (`cliente_id`) USING BTREE;

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `carrinho_compras`
--
ALTER TABLE `carrinho_compras`
  MODIFY `id_carrinho` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de tabela `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `estoque_materiais`
--
ALTER TABLE `estoque_materiais`
  MODIFY `id_estoque` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `estoque_movimentacao`
--
ALTER TABLE `estoque_movimentacao`
  MODIFY `id_estoque_movimentacao` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de tabela `itens_pedidos`
--
ALTER TABLE `itens_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de tabela `vendas`
--
ALTER TABLE `vendas`
  MODIFY `id_venda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de tabela `vendas_historico`
--
ALTER TABLE `vendas_historico`
  MODIFY `id_vendas_historico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `carrinho_compras`
--
ALTER TABLE `carrinho_compras`
  ADD CONSTRAINT `cariinho_compras_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `itens_pedidos`
--
ALTER TABLE `itens_pedidos`
  ADD CONSTRAINT `itens_pedidos_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `vendas`
--
ALTER TABLE `vendas`
  ADD CONSTRAINT `vendas_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `vendas_historico`
--
ALTER TABLE `vendas_historico`
  ADD CONSTRAINT `vendas_historico_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
